# Remi Chatbot

## Core Service

[![pipeline status](https://gitlab.com/adpro-a-11/tugas-kelompok/badges/core-service/pipeline.svg)](https://gitlab.com/adpro-a-11/tugas-kelompok/-/commits/core-service)
[![coverage report](https://gitlab.com/adpro-a-11/tugas-kelompok/badges/core-service/coverage.svg)](https://gitlab.com/adpro-a-11/tugas-kelompok/-/commits/core-service)

## Reminder Service

[![pipeline status](https://gitlab.com/adpro-a-11/tugas-kelompok/badges/reminder-service/pipeline.svg)](https://gitlab.com/adpro-a-11/tugas-kelompok/-/commits/reminder-service)
[![coverage report](https://gitlab.com/adpro-a-11/tugas-kelompok/badges/reminder-service/coverage.svg)](https://gitlab.com/adpro-a-11/tugas-kelompok/-/commits/reminder-service)

## Messaging Service
[![pipeline status](https://gitlab.com/adpro-a-11/tugas-kelompok/badges/messaging-service/pipeline.svg)](https://gitlab.com/adpro-a-11/tugas-kelompok/-/commits/messaging-service)
[![coverage report](https://gitlab.com/adpro-a-11/tugas-kelompok/badges/messaging-service/coverage.svg)](https://gitlab.com/adpro-a-11/tugas-kelompok/-/commits/messaging-service)


## Advance Programming Kelas A - Kelompok 11

### Member:
1. Dipta Laksmana Baswara - 1806235832
2. Khadijah Rizqy Mufida - 1806235712
3. Taufik Algi Fahri - 1806205136
4. Habel Christiando Tobing - 1806186761

### Project Description
This is a LINE chatbot used to make reminder or announcement to a group of people.

### Division of Tasks
- [x] Dipta Laksmana Baswara - ( Reminder System (Alert Message for H-7, H-1, T-30, ... , T-0), Add/Remove/Create Group and Group Statistic)
- [x] Habel Christiando Tobing (User Registration, Create Announcement)
- [ ] Taufik Algi Fahri (Group Survey)
- [x] Khadijah Rizqy Mufida (Create/Change/Remove/List Reminder)

## How To Use
1. Before using the bot please open the [service-registry](http://eureka-reminder.herokuapp.com) first
and then all the services
[core-service](http://remi-core.herokuapp.com/)
[reminder-service](http://remi-reminder.herokuapp.com/)
[messaging-service](http://remi-messaging.herokuapp.com/)

2. Add the bot with line@ id @967mybuy

3. Type /help for the instructions