package com.remi.core.command;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GroupCommand {
  /**
   * Part-code = .
   * 0 -> name
   * 1 -> desc
   * Format:
   * /create_group [name] - [desc:optional]
   * /update_group [value] [part-code]
   * /delete_group
   * /list_group_member
   * /detail_group
   * /count_group_reminder
   * /count_group_announcement
   * /announce_with_desc - [title] - [desc]
   * /announce_without_desc - [title]
   * /created_announcement_list
   * /reminder_with_desc - [title] - [desc] - [date] - [activation setting]
   * /reminder_without_desc - [title] - [date] - [activation setting]
   * /reminder_delete - [title]
   * /reminder_change_date - [title] - [date]
   * /reminder_change_description - [title] - [description]
   * /reminder_list_all_group
   */
  public static final List<String> listCommandAdmin =
      List.of(
          "/create_group",
          "/update_group",
          "/delete_group",
          "/list_group_member",
          "/detail_group",
          "/count_group_reminder",
          "/count_group_announcement",
          "/announce_with_desc",
          "/announce_without_desc",
          "/created_announcement_list",
          "/reminder_with_desc",
          "/reminder_without_desc",
          "/reminder_delete",
          "/reminder_change_date",
          "/reminder_change_description",
          "/reminder_list_all_group"
      );

  /**
   * Format :.
   * /list_group
   * /add_group [group-id]
   * /remove_group [group-id]
   * /detail_group [group-id]
   * /see_announcement [group-id] [title]
   * /available_announcement [group-id]
   * /reminder_list_all
   */
  public static final List<String> listCommandUser =
      List.of(
          "/list_group",
          "/add_group",
          "/remove_group",
          "/detail_group",
          "/see_announcement",
          "/available_announcement",
          "/reminder_list_all"
      );

  public static final List<String> listGroupCommand =
      Stream.concat(
          listCommandAdmin.stream(),
          listCommandUser.stream()
      ).collect(Collectors.toList());
}
