package com.remi.core.controller;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.proxy.user.UserProxy;
import com.remi.core.utils.PostBackMessageData;
import com.remi.core.utils.TextMessageData;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MainController {
  private MessagingFeign messagingFeign;

  @Autowired
  private UserProxy userProxy;


  /**
   * controller for input text message.
   *
   * @param data text message
   * @return string done
   */

  @PostMapping("/message/text")
  public String inputTextMessage(@RequestBody TextMessageData data) {
    String userId = data.getUserId();
    String replyToken = data.getReplyToken();
    String message = data.getMessage();

    String replyMessage;
    String[] arrMessage = message.split(" ");
    userProxy.checkMessage(data);

    return "Process Text Message Done";
  }

  /**
   * controller for input postback message.
   *
   * @param data postback message
   * @return string done
   */
  @PostMapping("/message/postback")
  public String inputPostBackMessage(@RequestBody PostBackMessageData data) {
    String userId = data.getUserId();
    String replyToken = data.getReplyToken();
    String postBackData = data.getPostBackData();
    Map<String, String> postBackParams = data.getPostBackParams();

    return "Process PostBack Message Done";
  }

}
