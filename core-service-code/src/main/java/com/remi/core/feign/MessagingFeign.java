package com.remi.core.feign;

import java.util.Map;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("remi-messaging")
public interface MessagingFeign {
  @PostMapping("/message/push")
  public String sendPushMessage(@RequestBody Map<String, String> data);

  @PostMapping("/message/reply")
  public String sendReplyMessage(@RequestBody Map<String, String> data);
}
