package com.remi.core.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcement")
public class Announcement {

  @Id
  @Column(name = "id")
  private String announcementId; //key format = <group_id>-<title>

  @Column(name = "description")
  private String description;

  @Column(name = "title")
  private String title;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "group_id", referencedColumnName = "id")
  private Group group;

  public Announcement() {

  }

  /**
   * Constructor for announcement.
   *
   * @param announcementId announcement id
   * @param description    announcement desc
   * @param title          announcement title
   * @param group          group announcement
   */
  public Announcement(String announcementId, String description, String title, Group group) {
    this.announcementId = announcementId;
    this.description = description;
    this.title = title;
    this.group = group;
  }

  public String getAnnouncementId() {
    return announcementId;
  }

  public void setAnnouncementId(String announcementId) {
    this.announcementId = announcementId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Group getGroup() {
    return group;
  }

  public void setGroup(Group group) {
    this.group = group;
  }
}
