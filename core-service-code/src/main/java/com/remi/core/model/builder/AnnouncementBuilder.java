package com.remi.core.model.builder;

import com.remi.core.model.Announcement;
import com.remi.core.model.Group;


public class AnnouncementBuilder {
  private String announcementId;
  private String description;
  private String title;
  private Group group;

  public AnnouncementBuilder setAnnouncementId(String announcementId) {
    this.announcementId = announcementId;
    return this;
  }

  public AnnouncementBuilder setDescription(String description) {
    this.description = description;
    return this;
  }

  public AnnouncementBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  public AnnouncementBuilder setGroup(Group group) {
    this.group = group;
    return this;
  }

  public String getTitle() {
    return this.title;
  }

  public Announcement createAnnouncement() {
    return new Announcement(this.announcementId, this.description, this.title, this.group);
  }
}




