package com.remi.core.model.builder;

import com.remi.core.model.Group;
import com.remi.core.model.User;
import java.util.Set;

public class GroupBuilder {

  private int groupId;
  private String groupName;
  private User user;
  private String name;
  private Set<User> groupMember;

  public GroupBuilder setGroupId(int groupId) {
    this.groupId = groupId;
    return this;
  }

  public GroupBuilder setGroupName(String groupName) {
    this.groupName = groupName;
    return this;
  }

  public GroupBuilder setUser(User user) {
    this.user = user;
    return this;
  }

  public GroupBuilder setDescription(String name) {
    this.name = name;
    return this;
  }

  public GroupBuilder setGroupMember(Set<User> groupMember) {
    this.groupMember = groupMember;
    return this;
  }

  public Group createGroup() {
    return new Group(groupId, groupName, user, name, groupMember);
  }

}
