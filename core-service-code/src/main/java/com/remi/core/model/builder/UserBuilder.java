package com.remi.core.model.builder;

import com.remi.core.model.Group;
import com.remi.core.model.User;
import java.util.Set;

public class UserBuilder {
  private String userId;
  private String name;
  private Set<Group> listGroup;

  public UserBuilder setUserId(String userId) {
    this.userId = userId;
    return this;
  }

  public UserBuilder setName(String name) {
    this.name = name;
    return this;
  }

  public UserBuilder setListGroup(Set<Group> listGroup) {
    this.listGroup = listGroup;
    return this;
  }

  public User createUser() {
    return new User(userId, name, listGroup);
  }

}
