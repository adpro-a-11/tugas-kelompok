package com.remi.core.model.repository;

import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, String> {

  int countAnnouncementsByGroup(Group group);

  List<Announcement> findAnnouncementsByGroup(Group group);

}
