package com.remi.core.model.repository;

import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {
  public Group getGroupByUser(User user);

  public boolean existsGroupsByUser(User user);

  public Group getGroupByGroupName(String name);

}
