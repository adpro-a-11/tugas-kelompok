package com.remi.core.model.repository;

import com.remi.core.model.Group;
import com.remi.core.model.Reminder;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReminderRepository extends JpaRepository<Reminder, String> {
  List<Reminder> findRemindersByDateTimeGreaterThanEqual(Date dateTime);

  List<Reminder> findRemindersByGroup(Group group);

  int countRemindersByGroupAndDateTimeGreaterThan(Group group, Date dateTime);

  int countRemindersByGroup(Group group);
}
