package com.remi.core.model.repository;

import com.remi.core.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

  @Query(value =
          "SELECT CASE WHEN COUNT(*) > 0 THEN true ELSE false END FROM LINE_USER as U "
                  + "WHERE U.USER_ID = ?1",
          nativeQuery = true)
  boolean existsUserByUserId(String id);

}
