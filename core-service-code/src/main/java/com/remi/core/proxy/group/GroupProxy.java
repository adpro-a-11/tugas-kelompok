package com.remi.core.proxy.group;

import com.remi.core.utils.PostBackMessageData;
import com.remi.core.utils.TextMessageData;

public interface GroupProxy {
  public void checkCommand(TextMessageData data);

  public void checkCommand(PostBackMessageData data);
}
