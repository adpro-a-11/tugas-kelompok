package com.remi.core.proxy.group;

import com.remi.core.command.GroupCommand;
import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.proxy.exception.group.AlreadyInTheGroupException;
import com.remi.core.proxy.exception.group.AlreadyOwnGroupException;
import com.remi.core.proxy.exception.group.NotGroupAdminException;
import com.remi.core.proxy.exception.group.NotGroupMemberException;
import com.remi.core.proxy.exception.group.UnknownCommandException;
import com.remi.core.proxy.exception.group.WrongFormatException;
import com.remi.core.service.announcement.AnnouncementService;
import com.remi.core.service.group.GroupService;
import com.remi.core.service.reminder.ReminderService;
import com.remi.core.utils.PostBackMessageData;
import com.remi.core.utils.TextMessageData;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class GroupProxyImpl implements GroupProxy {

  private final UserRepository userRepository;

  private final GroupService groupService;

  private final AnnouncementService announcementService;

  private final MessagingFeign messagingFeign;

  private final GroupRepository groupRepository;

  private final ReminderService reminderService;

  private final List<String> listCommandAdmin =
      GroupCommand.listCommandAdmin;

  private final List<String> listCommandUser =
      GroupCommand.listCommandUser;


  /**
   * Constructor.
   *
   * @param userRepository      bean
   * @param groupService        bean
   * @param announcementService bean
   * @param messagingFeign      bean
   * @param groupRepository     bean
   * @param reminderService     bean
   */
  public GroupProxyImpl(
      UserRepository userRepository,
      GroupService groupService,
      AnnouncementService announcementService,
      MessagingFeign messagingFeign,
      GroupRepository groupRepository, ReminderService reminderService) {
    this.userRepository = userRepository;
    this.groupService = groupService;
    this.announcementService = announcementService;
    this.messagingFeign = messagingFeign;
    this.groupRepository = groupRepository;
    this.reminderService = reminderService;
  }

  @Override
  public void checkCommand(PostBackMessageData data) {

  }

  @Override
  public void checkCommand(TextMessageData data) {
    try {
      String message = data.getMessage();
      String[] command = message.split(" ");
      if (!(listCommandAdmin.contains(command[0]) || listCommandUser.contains(command[0]))) {
        throw new UnknownCommandException();
      }
      if (command[0].equals("/detail_group")) {
        if (command.length == 2) {
          userCommand(data, command);
        } else if (command.length == 1) {
          adminCommand(data, command);
        } else {
          throw new WrongFormatException();
        }
      } else if (listCommandAdmin.contains(command[0])) {
        adminCommand(data, command);
      } else if (listCommandUser.contains(command[0])) {
        userCommand(data, command);
      }
    } catch (WrongFormatException e) {
      sendMessage(data, "Wrong Format");
    } catch (UnknownCommandException e) {
      sendMessage(data, "Unknown Command");
    }
  }

  private void adminCommand(TextMessageData data,
                            String[] command) throws WrongFormatException {
    User user = userRepository.getOne(
        data.getUserId()
    );
    String[] announcement = data.getMessage().split(" - ");
    int commandLength = command.length;
    try {
      if (command[0].equals("/create_group")) {
        if (groupRepository.existsGroupsByUser(user)) {
          throw new AlreadyOwnGroupException();
        } else {
          if (commandLength == 1) {
            throw new WrongFormatException();
          }
          String[] groupData =
              String.join(" ",
                  Arrays.copyOfRange(
                      command,
                      1,
                      commandLength
                  )
              ).split(" - ");
          int createGroupCommandLength =
              groupData.length;

          switch (createGroupCommandLength) {
            case 1:
              groupService.createGroup(
                  user,
                  data.getReplyToken(),
                  groupData[0]);
              break;
            case 2:
              groupService.createGroup(
                  user,
                  data.getReplyToken(),
                  groupData[0],
                  groupData[1]
              );
              break;
            default:
              throw new WrongFormatException();
          }
        }
      } else {
        if (!groupRepository.existsGroupsByUser(user)) {
          throw new NotGroupAdminException();
        }
        final List<Runnable> listFunctionAdmin =
            List.of(
                () -> {
                  try {
                    if (commandLength < 3) {
                      throw new IndexOutOfBoundsException();
                    }
                    int partCode = Integer.parseInt(command[commandLength - 1]);
                    if (!(partCode == 0 || partCode == 1)) {
                      throw new WrongFormatException();
                    }
                    groupService.updateGroup(
                        user,
                        data.getReplyToken(),
                        String.join(" ",
                            Arrays.copyOfRange(command, 1, commandLength - 1)),
                        partCode
                    );
                  } catch (NumberFormatException e) {
                    sendMessage(data, "Part code must be a number");
                  } catch (WrongFormatException e) {
                    sendMessage(data, "Wrong part code");
                  }
                },
                () -> {
                  groupService.deleteGroup(
                      user,
                      data.getReplyToken()
                  );
                },
                () -> {
                  groupService.listGroupMember(
                      user,
                      data.getReplyToken()
                  );
                },
                () -> {
                  groupService.detailGroup(
                      user,
                      data.getReplyToken()
                  );
                },
                () -> {
                  groupService.countGroupReminder(
                      user,
                      data.getReplyToken()
                  );
                },
                () -> {
                  groupService.countGroupAnnouncement(
                      user,
                      data.getReplyToken()
                  );
                },
                () -> {
                  announcementService.createAnnouncement(
                      user,
                      data.getReplyToken(),
                      true,
                      announcement[1],
                      announcement[2]
                  );
                },
                () -> {
                  announcementService.createAnnouncement(
                      user,
                      data.getReplyToken(),
                      false,
                      announcement[1],
                      " "
                  );
                },
                () -> {
                  announcementService.listOfAnnouncementsByAdmin(
                      data
                  );
                },
                () -> {
                  Group group = groupRepository.getGroupByUser(user);
                  String reminderId =
                      reminderService.createReminderId(group, announcement[1]);
                  reminderService.createReminderByReminderId(
                      reminderId, user, data.getReplyToken(), true,
                      announcement[2], announcement[3], announcement[4]);
                },
                () -> {
                  Group group = groupRepository.getGroupByUser(user);
                  String reminderId =
                      reminderService.createReminderId(group, announcement[1]);
                  reminderService.createReminderByReminderId(
                      reminderId, user, data.getReplyToken(), false,
                      "", announcement[2], announcement[3]);
                },
                () -> {
                  Group group = groupRepository.getGroupByUser(user);
                  String reminderId =
                      reminderService.createReminderId(group, announcement[1]);
                  reminderService.deleteReminder(data.getReplyToken(), reminderId);
                },
                () -> {
                  Group group = groupRepository.getGroupByUser(user);
                  String reminderId =
                      reminderService.createReminderId(group, announcement[1]);
                  reminderService.changeReminderDate(
                      data.getReplyToken(), reminderId, announcement[2]);
                },
                () -> {
                  Group group = groupRepository.getGroupByUser(user);
                  String reminderId =
                      reminderService.createReminderId(group, announcement[1]);
                  reminderService.changeReminderDescription(
                      data.getReplyToken(), reminderId, announcement[2]);
                },
                () -> {
                  reminderService.listReminderByAdmin(data);
                }
            );
        for (int i = 1; i < listCommandAdmin.size(); i++) {
          String commandString = listCommandAdmin.get(i);
          String userCommand = command[0];
          if (userCommand.equals(commandString)) {
            try {
              listFunctionAdmin.get(i - 1).run();
            } catch (IndexOutOfBoundsException e) {
              throw new WrongFormatException();
            }
            break;
          }
        }
      }
    } catch (NotGroupAdminException e) {
      sendMessage(data, "You don't own a group");
    } catch (AlreadyOwnGroupException e) {
      sendMessage(data, "You already own a group");
    }

  }

  private void userCommand(TextMessageData data, String[] command) throws WrongFormatException {
    User user = userRepository.getOne(
        data.getUserId()
    );
    if (command[0].equals("/list_group")) {
      groupService.listGroup(user, data.getReplyToken());
    } else if (command[0].equals("/reminder_list_all")) {
      reminderService.listReminderByUser(data, data.getUserId());
    } else {
      try {
        int groupId = Integer.parseInt(command[1]);
        Group group = groupRepository.getOne(groupId);
        final List<Runnable> listFunction =
            List.of(
                () -> {
                  groupService.removeGroup(
                      user,
                      data.getReplyToken(),
                      group);
                },
                () -> {
                  groupService.detailGroup(
                      user,
                      data.getReplyToken(),
                      group
                  );
                },
                () -> {
                  if (command.length >= 3) {
                    announcementService.seeAnnouncement(
                        data.getReplyToken(),
                        groupId,
                        String.join(" ",
                            Arrays.copyOfRange(command, 2, command.length))
                    );
                  } else {
                    sendMessage(data, "Wrong Format");
                  }
                },
                () -> {
                  announcementService.listOfAnnouncementsByUser(
                      data,
                      command[1]
                  );
                }
            );
        if (command[0].equals("/add_group")) {
          if (isUserMemberOfAGroup(user, group)) {
            throw new AlreadyInTheGroupException();
          }
          groupService.addGroup(
              user,
              data.getReplyToken(),
              group);
        } else {
          if (!isUserMemberOfAGroup(user, group)) {
            throw new NotGroupMemberException();
          }
          String userCommand = command[0];
          for (int i = 2; i < listCommandUser.size(); i++) {
            String commandString = listCommandUser.get(i);
            if (userCommand.equals(commandString)) {
              listFunction.get(i - 2).run();
              break;
            }
          }
        }
      } catch (NumberFormatException e) {
        sendMessage(data, "Group id must be a number");
      } catch (NotGroupMemberException e) {
        sendMessage(data, "You're not member of that group");
      } catch (AlreadyInTheGroupException e) {
        sendMessage(data, "Already be a member in the group");
      } catch (IndexOutOfBoundsException e) {
        throw new WrongFormatException();
      }
    }
  }

  private void sendMessage(TextMessageData data, String message) {
    HashMap<String, String> dataMessage = new HashMap<>();
    dataMessage.put(data.getReplyToken(), message);
    messagingFeign.sendReplyMessage(
        dataMessage
    );
  }

  private boolean isUserMemberOfAGroup(User user, Group group) {
    Set<User> groupMember = group.getGroupMember();
    return groupMember.stream().anyMatch(
        obj -> obj.getUserId()
            .equals(
                user.getUserId()
            )
    );
  }
}
