package com.remi.core.proxy.user;

import com.remi.core.utils.TextMessageData;

public interface UserProxy {

  void checkMessage(TextMessageData data);

}

