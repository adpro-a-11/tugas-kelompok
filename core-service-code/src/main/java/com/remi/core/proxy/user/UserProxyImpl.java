package com.remi.core.proxy.user;

import com.remi.core.command.GroupCommand;
import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.User;
import com.remi.core.model.builder.UserBuilder;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.proxy.group.GroupProxy;
import com.remi.core.utils.TextMessageData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserProxyImpl implements UserProxy {
  private UserRepository userRepository;
  private MessagingFeign messagingFeign;
  private GroupProxy groupProxy;

  /**
   * User's List of Announcements in A Given GroupID.
   */
  public UserProxyImpl(UserRepository userRepository, MessagingFeign messagingFeign,
                       GroupProxy groupProxy) {
    this.userRepository = userRepository;
    this.messagingFeign = messagingFeign;
    this.groupProxy = groupProxy;
  }

  /**
   * method to parse incoming messages.
   */

  public void checkMessage(TextMessageData data) {
    String message;
    if (!(userRepository.existsById(data.getUserId()))) {
      String[] command = data.getMessage().split(" ");
      switch (command[0]) {

        case ("/help"):
          message = "Please register first with this format: "
            + "\n /register [name] \n ex: /register Remi";
          sendMessage(data.getReplyToken(), message);
          break;

        case ("/register"):
          UserBuilder userBuilder = new UserBuilder();
          userBuilder.setUserId(data.getUserId());
          userBuilder.setName(command[1]);
          User user = userBuilder.createUser();
          userRepository.save(user);

          message = String.format("Welcome, %s!", user.getName());
          sendMessage(data.getReplyToken(), message);
          break;

        default:
          messageIssues(data.getReplyToken());
          break;
      }
    } else {
      String[] command = data.getMessage().split(" ");
      List<String> groupCommand = GroupCommand.listGroupCommand;
      if (groupCommand.contains(command[0])) {
        groupProxy.checkCommand(data);
      } else {
        switch (command[0]) {

          case ("/help"):
            message = "List of available commands with formats: \n \n"

              + "\nAdmin: \n "
              + "\n/create_group [name] - [desc:optional]\n"
              + "\n/update_group [value] [part-code]\n"
              + "note: replace title(part-code = 0) or desc(part-code = 1) with value\n"
              + "\n/delete_group\n"
              + "\n/list_group_member\n"
              + "\n/detail_group\n"
              + "\n/count_group_reminder\n"
              + "\n/count_group_announcement\n"
              + "\n/announce_with_desc - [title] - [desc]\n"
              + "\n/announce_without_desc - [title]\n"
              + "\n/created_announcement_list\n\n"
              + "note: \n"
              + "date format: dd-MM-yyyy HH:mm\n"
              + "activation settings : T-3 (3 minute before), "
              + "T-5 (5 minute before) , "
              + "H-7 (7 days before)\n\n"
              + "\n/reminder_with_desc - [title] - [desc] - [date] - [activation setting]\n"
              + "\n/reminder_without_desc - [title] - [date] - [activation setting]\n"
              + "\n/reminder_delete - [title]\n"
              + "\n/reminder_change_date - [title] - [date]\n"
              + "\n/reminder_change_description - [title] - [description]\n"
              + "\n/reminder_list_all_group\n \n"

              + "\nUser: \n "
              + "\n/list_group\n"
              + "\n/add_group [group-id]\n"
              + "\n/remove_group [group-id]\n"
              + "\n/detail_group [group-id]\n"
              + "\n/see_announcement [group-id] [title]\n"
              + "\n/available_announcement [group-id]\n"
              + "\n/reminder_list_all";

            Map<String, String> help = new HashMap<>();
            help.put(data.getReplyToken(), message);
            messagingFeign.sendReplyMessage(help);
            break;

          default:
            messageIssues(data.getReplyToken());
            break;
        }
      }
    }
  }

  /**
   * method to remind users to see the list of commands.
   */
  public void messageIssues(String replyToken) {
    String message;
    message = "Please type /help for instructions";
    Map<String, String> response = new HashMap<>();
    response.put(replyToken, message);
    messagingFeign.sendReplyMessage(response);
  }

  private void sendMessage(String replyToken, String message) {
    HashMap<String, String> data = new HashMap<>();
    data.put(replyToken, message);
    messagingFeign.sendReplyMessage(
        data
    );
  }
}