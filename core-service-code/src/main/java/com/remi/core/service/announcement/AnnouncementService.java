package com.remi.core.service.announcement;

import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.utils.TextMessageData;

public interface AnnouncementService {
  public void createAnnouncement(User admin, String replyToken, boolean descriptionFlag,
                                 String title, String description);

  public void seeAnnouncement(String replyToken, int groupId, String title);

  public void listOfAnnouncementsByAdmin(TextMessageData data);

  public void listOfAnnouncementsByUser(TextMessageData data, String id);

  public void announcementListHelper(TextMessageData data, Group group);
}
