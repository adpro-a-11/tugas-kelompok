package com.remi.core.service.announcement;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.builder.AnnouncementBuilder;
import com.remi.core.model.repository.AnnouncementRepository;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.utils.TextMessageData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AnnouncementServiceImpl implements AnnouncementService {

  private  AnnouncementBuilder announcementBuilder;

  private AnnouncementRepository announcementRepository;

  private UserRepository userRepository;

  private GroupRepository groupRepository;

  private MessagingFeign messagingFeign;

  /**
   * Constructor.
   *
   * @param announcementRepository announcementRepository
   * @param userRepository userRepository
   * @param groupRepository groupRepository
   * @param messagingFeign message.
   */
  public AnnouncementServiceImpl(AnnouncementRepository announcementRepository,
                                 UserRepository userRepository, GroupRepository groupRepository,
                                 MessagingFeign messagingFeign) {
    this.announcementRepository = announcementRepository;
    this.userRepository = userRepository;
    this.groupRepository = groupRepository;
    this.messagingFeign = messagingFeign;
  }

  /**
   * Create Announcement.
   *
   * @param admin      admin of the group
   * @param replyToken      replyToken from admin
   * @param descriptionFlag true -> announcement with description
   * @param title      announcement's title
   * @param description       announcement's description.
   */
  @Override
  public void createAnnouncement(User admin, String replyToken, boolean descriptionFlag,
                                 String title, String description) {
    String message;
    String messageAdmin;
    announcementBuilder = new AnnouncementBuilder();
    Group group = groupRepository.getGroupByUser(admin);
    announcementBuilder.setTitle(title);
    announcementBuilder.setAnnouncementId(String.valueOf(group.getGroupId())
              + "-" + announcementBuilder.getTitle());
    announcementBuilder.setGroup(group);
    if (descriptionFlag) {
      announcementBuilder.setDescription(description);
      message = String.format("Title: %s \n Description: %s", title, description);
      messageAdmin = String.format("Announcement titled: %s has been created", title);
    } else {
      message = String.format("Title: %s", title);
      messageAdmin = String.format("Announcement titled: %s "
              + "has been created (without description)", title);
    }
    Announcement announcement = announcementBuilder.createAnnouncement();
    announcementRepository.save(announcement);

    Map<String, String> announce = new HashMap<>();
    for (User user : group.getGroupMember()) {
      String userId = user.getUserId();
      announce.put(userId, message);
    }
    sendMessage(replyToken, messageAdmin);
    messagingFeign.sendPushMessage(announce);
  }

  /**
   * See Announcement By ID.
   *
   * @param replyToken     message token
   * @param groupId group's id.
   * @param title announcement's title.
   */
  @Override
  public void seeAnnouncement(String replyToken, int groupId, String title) {
    Announcement announcement =
        announcementRepository.getOne(String.valueOf(groupId) + "-" + title);
    String message = String.format("Title: %s \n Description: %s", announcement.getTitle(),
            announcement.getDescription());
    sendMessage(replyToken, message);
  }

  /**
   * Admin's List of Announcements.
   *
   * @param data      message from admin
   */
  @Override
  public void listOfAnnouncementsByAdmin(TextMessageData data) {
    User admin = userRepository.getOne(data.getUserId());
    Group group = groupRepository.getGroupByUser(admin);

    announcementListHelper(data, group);

  }

  /**
   * User's List of Announcements in A Given GroupID.
   *
   * @param data      message from user
   * @param id      group ID.
   */
  @Override
  public void listOfAnnouncementsByUser(TextMessageData data, String id) {
    Group group = groupRepository.getOne(Integer.parseInt(id));

    announcementListHelper(data, group);

  }

  /**
   * For helping viewing list of announcements.
   *
   * @param data      message from user
   * @param group      group source.
   */
  public void announcementListHelper(TextMessageData data, Group group) {
    List<Announcement> announcementList = announcementRepository.findAnnouncementsByGroup(group);

    StringBuilder message = new StringBuilder("List of Announcements:\n");
    int counter = 1;
    for (Announcement announcement :
            announcementList) {
      message.append(
              String.format("%d. %s - ID: %s\n",
                      counter++, announcement.getTitle(),
                      String.valueOf(announcement.getAnnouncementId())
              )
      );
    }
    sendMessage(data.getReplyToken(), message.toString());
  }


  private void sendMessage(String replyToken, String message) {
    HashMap<String, String> data = new HashMap<>();
    data.put(replyToken, message);
    messagingFeign.sendReplyMessage(
            data
    );
  }


}
