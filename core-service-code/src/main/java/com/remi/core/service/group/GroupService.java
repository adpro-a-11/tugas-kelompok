package com.remi.core.service.group;

import com.remi.core.model.Group;
import com.remi.core.model.User;

public interface GroupService {
  public void createGroup(User user, String replyToken, String groupName, String description);

  public void createGroup(User user, String replyToken, String groupName);

  public void deleteGroup(User user, String replyToken);

  public void updateGroup(User user, String replyToken, String value, int part);

  public void addGroup(User user, String replyToken, Group group);

  public void removeGroup(User user, String replyToken, Group group);

  public void detailGroup(User user, String replyToken);

  public void detailGroup(User user, String replyToken, Group group);

  public void listGroupMember(User user, String replyToken);

  public void countGroupReminder(User user, String replyToken);

  public void countGroupAnnouncement(User user, String replyToken);

  public void listGroup(User user, String replyToken);
}
