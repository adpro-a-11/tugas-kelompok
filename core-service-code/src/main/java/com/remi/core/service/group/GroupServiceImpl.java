package com.remi.core.service.group;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.repository.AnnouncementRepository;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.ReminderRepository;
import com.remi.core.utils.DateUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class GroupServiceImpl implements GroupService {

  private final GroupRepository groupRepository;

  private final MessagingFeign messagingFeign;

  private final ReminderRepository reminderRepository;

  private final AnnouncementRepository announcementRepository;

  /**
   * Constructor.
   * @param groupRepository bean
   * @param messagingFeign bean
   * @param reminderRepository bean
   * @param announcementRepository bean
   */
  public GroupServiceImpl(
          GroupRepository groupRepository,
          MessagingFeign messagingFeign,
          ReminderRepository reminderRepository,
          AnnouncementRepository announcementRepository) {
    this.groupRepository = groupRepository;
    this.messagingFeign = messagingFeign;
    this.reminderRepository = reminderRepository;
    this.announcementRepository = announcementRepository;
  }

  @Override
  public void createGroup(User user, String replyToken, String groupName, String description) {
    Group group = new Group();
    group.setGroupMember(new HashSet<>());
    group.setGroupName(groupName);
    group.setDescription(description);
    group.setUser(user);
    groupRepository.save(group);
    sendMessage(replyToken, String.format("Group %s created", groupName));
  }

  @Override
  public void createGroup(User user, String replyToken, String groupName) {
    Group group = new Group();
    group.setGroupMember(new HashSet<>());
    group.setGroupName(groupName);
    group.setDescription("");
    group.setUser(user);
    groupRepository.save(group);
    sendMessage(replyToken, String.format("Group %s created", groupName));
  }

  @Override
  public void deleteGroup(User user, String replyToken) {
    Group group = groupRepository.getGroupByUser(user);
    groupRepository.deleteById(group.getGroupId());
    sendMessage(replyToken,
            String.format(
                    "Success delete group %s", group.getGroupName()
            )
    );
  }

  /**
   * Update group detail.
   *
   * @param user       user object
   * @param replyToken replyToken
   * @param value      new value
   * @param part       code part 0 -> name 1 -> desc
   */
  @Override
  public void updateGroup(User user, String replyToken, String value, int part) {
    Group group = groupRepository.getGroupByUser(user);
    switch (part) {
      case 0:
        group.setGroupName(value);
        break;
      case 1:
        group.setDescription(value);
        break;
      default:
        break;
    }
    groupRepository.save(group);
    sendMessage(replyToken,
            String.format(
                    "Change group %s to %s",
                    part == 0 ? "name" : "description",
                    value
            )
    );
  }

  @Override
  public void addGroup(User user, String replyToken, Group group) {
    group.addGroupMember(user);
    groupRepository.save(group);
    sendMessage(replyToken,
            String.format(
                    "Added to group %s",
                    group.getGroupName()
            )
    );
  }

  @Override
  public void removeGroup(User user, String replyToken, Group group) {
    group.removeGroupMember(user);
    groupRepository.save(group);
    sendMessage(replyToken,
            String.format(
                    "Removed from group %s",
                    group.getGroupName()
            )
    );
  }

  @Override
  public void detailGroup(User user, String replyToken) {
    Group group = groupRepository.getGroupByUser(user);
    DateUtils dateUtils = DateUtils.getInstance();
    String message =
            String.format(
                    "Group %s - Id %d\n"
                            +
                            "Description :\n"
                            +
                            "%s\n"
                            +
                            "Total Member : %d\n"
                            +
                            "Total Reminder : %d\n"
                            +
                            "Active Reminder : %d\n"
                            +
                            "Total Announcement : %d\n",
                    group.getGroupName(),
                    group.getGroupId(),
                    group.getDescription(),
                    group.getGroupMemberSize(),
                    reminderRepository.countRemindersByGroup(group),
                    reminderRepository
                            .countRemindersByGroupAndDateTimeGreaterThan(
                                    group, dateUtils.normalizeDate(new Date())),
                    announcementRepository.countAnnouncementsByGroup(group)
            );
    sendMessage(replyToken, message);
  }

  @Override
  public void detailGroup(User user, String replyToken, Group group) {
    DateUtils dateUtils = DateUtils.getInstance();
    String message =
            String.format(
                    "Group %s - Id %d\n"
                            +
                            "Description :\n"
                            +
                            "%s\n"
                            +
                            "Member : %d\n"
                            +
                            "Active Reminder : %d",
                    group.getGroupName(),
                    group.getGroupId(),
                    group.getDescription(),
                    group.getGroupMemberSize(),
                    reminderRepository
                            .countRemindersByGroupAndDateTimeGreaterThan(
                                    group, dateUtils.normalizeDate(new Date()))
            );
    sendMessage(replyToken, message);
  }

  @Override
  public void listGroupMember(User user, String replyToken) {
    Group group = groupRepository.getGroupByUser(user);
    Set<User> userMember = group.getGroupMember();
    StringBuilder message = new StringBuilder("Group Member :\n");
    int counter = 1;
    for (User member :
            userMember) {
      message.append(String.format("%d. %s\n", counter++, member.getName()));
    }
    sendMessage(replyToken, message.toString());
  }

  @Override
  public void countGroupReminder(User user, String replyToken) {
    DateUtils dateUtils = DateUtils.getInstance();
    Group group = groupRepository.getGroupByUser(user);
    int reminderCount = reminderRepository.countRemindersByGroup(group);
    int activeReminderCount =
            reminderRepository
                    .countRemindersByGroupAndDateTimeGreaterThan(group,
                            dateUtils.normalizeDate(new Date()));
    String message = String.format(
            "Group %s\n"
                    +
                    "Active reminder : %d\n"
                    +
                    "Total Reminder : %d",
            group.getGroupName(),
            activeReminderCount,
            reminderCount
    );
    sendMessage(replyToken, message);
  }

  @Override
  public void countGroupAnnouncement(User user, String replyToken) {
    Group group = groupRepository.getGroupByUser(user);
    int announcementCount =
            announcementRepository.countAnnouncementsByGroup(group);
    String message = String.format(
            "Group %s\n"
                    +
                    "Total Announcement : %d",
            group.getGroupName(),
            announcementCount
    );
    sendMessage(replyToken, message);
  }

  @Override
  public void listGroup(User user, String replyToken) {
    Set<Group> groupList = user.getListGroup();
    if (groupList.isEmpty()) {
      sendMessage(replyToken, "You don't join to any group");
    } else {
      StringBuilder message = new StringBuilder("List Group :\n");
      int counter = 1;
      for (Group group :
              groupList) {
        message.append(
                String.format("%d. %s - Id : %d\n",
                        counter++, group.getGroupName(), group.getGroupId()
                )
        );
      }
      sendMessage(replyToken, message.toString());
    }
  }

  private void sendMessage(String replyToken, String message) {
    HashMap<String, String> data = new HashMap<>();
    data.put(replyToken, message);
    messagingFeign.sendReplyMessage(
            data
    );
  }
}
