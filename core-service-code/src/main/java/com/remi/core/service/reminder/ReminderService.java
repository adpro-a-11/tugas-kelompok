package com.remi.core.service.reminder;

import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.utils.TextMessageData;

public interface ReminderService {
  public String createReminderId(Group group, String title);

  public void createReminderByReminderId(String reminderId, User admin, String replyToken,
                                         boolean descriptionFlag, String description,
                                         String datetime, String activationSetting);

  public void changeReminderDate(String replyToken, String reminderId,
                                 String datetime);

  public void changeReminderDescription(String replyToken, String reminderId,
                                        String description);

  public void deleteReminder(String replyToken, String reminderId);

  public void listReminderByAdmin(TextMessageData data);

  public void listReminderByUser(TextMessageData data, String id);
}
