package com.remi.core.service.reminder;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Group;
import com.remi.core.model.Reminder;
import com.remi.core.model.User;
import com.remi.core.model.builder.ReminderBuilder;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.ReminderRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.utils.TextMessageData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ReminderServiceImpl implements ReminderService {
  private ReminderBuilder reminderBuilder;

  private GroupRepository groupRepository;

  private ReminderRepository reminderRepository;

  private UserRepository userRepository;

  private MessagingFeign messagingFeign;

  /**
   * Constructor.
   *
   * @param reminderRepository non empty.
   * @param userRepository     non empty.
   * @param groupRepository    non empty.
   * @param messagingFeign     non empty.
   */
  public ReminderServiceImpl(ReminderRepository reminderRepository,
                             UserRepository userRepository,
                             GroupRepository groupRepository,
                             MessagingFeign messagingFeign) {
    this.reminderRepository = reminderRepository;
    this.userRepository = userRepository;
    this.groupRepository = groupRepository;
    this.messagingFeign = messagingFeign;
  }

  /**
   * A method to create reminder.
   *
   * @param group not empty
   * @param title not empty
   */
  @Override
  public String createReminderId(Group group, String title) {
    String reminderId = group.getGroupId() + "-" + title;
    return reminderId;
  }

  /**
   * create reminder.
   *
   * @param reminderId        non empty.
   * @param admin             non empty.
   * @param replyToken        non empty.
   * @param descriptionFlag   non empty.
   * @param description       non empty.
   * @param datetime          non empty.
   * @param activationSetting non empty.
   */
  @Override
  public void createReminderByReminderId(String reminderId, User admin, String replyToken,
                                         boolean descriptionFlag, String description,
                                         String datetime, String activationSetting) {
    try {
      String message;
      reminderBuilder = new ReminderBuilder();
      Group group = groupRepository.getGroupByUser(admin);
      String title = reminderId.split("-")[1];
      reminderBuilder.setTitle(title);
      reminderBuilder.setReminderId(reminderId);
      reminderBuilder.setGroup(group);
      if (descriptionFlag) {
        reminderBuilder.setDescription(description);
        message = String.format("Reminder with Title: %s and Description: %s"
+
          " created on %s", title, description, datetime);
      } else {
        reminderBuilder.setDescription("None");
        message = String.format("Reminder with Title: %s created on %s",
          title, datetime);
      }
      String[] split = activationSetting.split(" ");
      List<String> activationSettingList = new ArrayList<>(Arrays.asList(split));
      reminderBuilder.setActivationSetting(activationSettingList);
      Date date = parseDate(datetime);
      reminderBuilder.setDateTime(date);
      Reminder reminder = reminderBuilder.createReminder();
      reminderRepository.save(reminder);
      sendMessage(replyToken, message);
    } catch (ParseException e) {
      sendMessage(replyToken, "Wrong Date Format must be dd-MM-yyyy HH:mm");
    }
  }

  /**
   * change reminder's date.
   *
   * @param replyToken not empty
   * @param reminderId not empty
   * @param datetime   not empty
   */
  @Override
  public void changeReminderDate(String replyToken,
                                 String reminderId, String datetime) {
    try {
      Reminder reminder = reminderRepository.getOne(reminderId);
      String pastDateTime = reminder.getDateTime().toString();
      Date date = parseDate(datetime);
      reminder.setDateTime(date);
      reminderRepository.save(reminder);
      String message = String.format("Reminder with "
          + "title: %s has been changed from %s to %s",
          reminder.getTitle(), pastDateTime,
          datetime);
      sendMessage(replyToken, message);
    } catch (ParseException e) {
      sendMessage(replyToken, "Wrong Date Format must be dd-MM-yyyy HH:mm");
    }
  }

  /**
   * A method to change reminder's description.
   *
   * @param replyToken  not empty
   * @param reminderId  not empty
   * @param description not empty
   */
  @Override
  public void changeReminderDescription(String replyToken, String reminderId,
                                        String description) {
    Reminder reminder = reminderRepository.getOne(reminderId);
    String pastDescription = reminder.getDescription();
    reminder.setDescription(description);
    reminderRepository.save(reminder);
    String message = String.format("Reminder with title: %s has been changed "
        + "from %s to %s", reminder.getTitle(), pastDescription, description);
    sendMessage(replyToken, message);
  }

  /**
   * a method to delete reminder.
   *
   * @param replyToken not empty.
   * @param reminderId not empty.
   */
  @Override
  public void deleteReminder(String replyToken, String reminderId) {
    String title = reminderRepository.getOne(reminderId).getTitle();
    reminderRepository.deleteById(reminderId);
    String message = "Reminder" + title + " is successfully deleted";
    sendMessage(replyToken, message);
  }

  /**
   * a method to list reminders for the group.
   *
   * @param data non empty.
   */
  @Override
  public void listReminderByAdmin(TextMessageData data) {
    User admin = userRepository.getOne(data.getUserId());
    Group group = groupRepository.getGroupByUser(admin);
    List<Reminder> reminderList = reminderRepository.findRemindersByGroup(group);

    StringBuilder message = new StringBuilder(
        "List of Reminders in Group "
        + group.getGroupName() + ":\n");
    int counter = 1;
    for (Reminder reminder : reminderList) {
      message.append(
          String.format("%d. %s - ID: %s\n",
          counter++, reminder.getTitle(),
          reminder.getReminderId())
      );
    }
    sendMessage(data.getReplyToken(), message.toString());
  }

  /**
   * to see the list of reminders by user.
   *
   * @param data non empty.
   * @param userId non empty.
   */
  @Override
  public void listReminderByUser(TextMessageData data, String userId) {
    User user = userRepository.getOne(userId);

    StringBuilder message = new StringBuilder("All Your Reminders\n\n");
    for (Group group: user.getListGroup()) { 
      message.append("List of "
              + "Reminders in Group "
              + group.getGroupName() + ":\n");
      List<Reminder> reminderList = reminderRepository.findRemindersByGroup(group);
      int counter = 1;
      for (Reminder reminder : reminderList) {
        message.append(
            String.format("%d. %s - ID: %s\n",
            counter++, reminder.getTitle(),
            reminder.getReminderId())
        );
      }
      message.append("\n");
    }
    sendMessage(data.getReplyToken(), message.toString());
  }

  private void sendMessage(String replyToken, String message) {
    HashMap<String, String> data = new HashMap<>();
    data.put(replyToken, message);
    messagingFeign.sendReplyMessage(data);
  }

  private Date parseDate(String date) throws ParseException {
    return new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(date);
  }
}