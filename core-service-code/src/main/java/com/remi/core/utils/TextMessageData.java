package com.remi.core.utils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class TextMessageData extends UserData {
  private String message;

  public TextMessageData(String userId, String replyToken, String message) {
    super(userId, replyToken);
    this.message = message;
  }
}
