package com.remi.core.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.remi.core.proxy.user.UserProxyImpl;
import com.remi.core.utils.PostBackMessageData;
import com.remi.core.utils.TextMessageData;

import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(controllers = MainController.class)
public class MainControllerTest {

  @MockBean
  private UserProxyImpl userProxy;

  @Autowired
  public MockMvc mockMvc;

  @Test
  public void inputTextMessage() throws Exception{
    ObjectMapper mapper = new ObjectMapper();
    TextMessageData textMessageData = new TextMessageData(
      "asd",
      "asd",
      "asda"
    );
    String data = mapper.writeValueAsString(textMessageData);

    mockMvc.perform(post("/message/text")
      .content(data).contentType(MediaType.APPLICATION_JSON))
      .andDo((res) -> {
        assertEquals("Process Text Message Done",res.getResponse().getContentAsString());
      });
  }

  @Test
  public void inputPostBackMessage() throws Exception{
    ObjectMapper mapper = new ObjectMapper();
    PostBackMessageData postBackMessageData = new PostBackMessageData(
      "asd",
      "asd",
      new HashMap<>(),
      "asd"
    );
    String data = mapper.writeValueAsString(postBackMessageData);

    mockMvc.perform(post("/message/postback")
      .content(data).contentType(MediaType.APPLICATION_JSON))
      .andDo((res) -> {
        assertEquals("Process PostBack Message Done",res.getResponse().getContentAsString());
      });
  }
}
