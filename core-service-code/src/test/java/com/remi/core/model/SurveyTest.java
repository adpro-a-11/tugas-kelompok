package com.remi.core.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SurveyTest {

  @Mock
  private Survey survey;


  @Test
  void getSurveyId() {
    Mockito.doReturn("asdasd").when(survey).getSurveyId();
    assertEquals("asdasd", survey.getSurveyId());
  }

  @Test
  void setSurveyId() {
    Mockito.doCallRealMethod().when(survey).setSurveyId("asdasd");
    Mockito.doCallRealMethod().when(survey).getSurveyId();
    survey.setSurveyId("asdasd");
    assertEquals("asdasd", survey.getSurveyId());
  }

  @Test
  void getQuestion() {
    Mockito.doReturn("asdasd").when(survey).getQuestion();
    assertEquals("asdasd", survey.getQuestion());
  }

  @Test
  void setQuestion() {
    Mockito.doCallRealMethod().when(survey).setQuestion("asdasd");
    Mockito.doCallRealMethod().when(survey).getQuestion();
    survey.setQuestion("asdasd");
    assertEquals("asdasd", survey.getQuestion());
  }

  @Test
  void getGroup() {
    Group group = new Group();
    Mockito.doReturn(group).when(survey).getGroup();
    assertEquals(group, survey.getGroup());
  }

  @Test
  void setGroup() {
    Group group = new Group();
    Mockito.doCallRealMethod().when(survey).setGroup(group);
    Mockito.doCallRealMethod().when(survey).getGroup();
    survey.setGroup(group);
    assertEquals(group, survey.getGroup());
  }

  @Test
  void getTitle() {
    Mockito.doReturn("asdasd").when(survey).getQuestion();
    assertEquals("asdasd", survey.getQuestion());
  }

  @Test
  void setTitle() {
    Mockito.doCallRealMethod().when(survey).setTitle("asdasd");
    Mockito.doCallRealMethod().when(survey).getTitle();
    survey.setTitle("asdasd");
    assertEquals("asdasd", survey.getTitle());
  }
}