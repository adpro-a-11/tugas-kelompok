package com.remi.core.model;

import com.remi.core.model.builder.GroupBuilder;
import com.remi.core.model.builder.UserBuilder;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

  private User user;

  @BeforeEach
  void setUp() {
    user = new User();
  }

  @Test
  void addGroupTest() {
    Group group = new GroupBuilder().createGroup();
    User user = new UserBuilder().createUser();
    group.setGroupMember(new HashSet<>());
    user.setListGroup(new HashSet<>());
    user.addGroup(group);

    assertEquals(1, user.getListGroupSize());
  }

  @Test
  void removeGroupTest() {
    Group group = new GroupBuilder().createGroup();
    User user = new UserBuilder().createUser();
    group.setGroupMember(new HashSet<>());
    user.setListGroup(new HashSet<>());
    user.addGroup(group);

    user.removeGroup(group);
    assertEquals(0, user.getListGroupSize());
  }

  @Test
  void getName() {
    user.setName("asdasd");
    assertEquals("asdasd", user.getName());
  }

  @Test
  void setName() {
    user.setName("asdasd");
    assertEquals("asdasd", user.getName());
  }

  @Test
  void getUserId() {
    user.setUserId("asdasd");
    assertEquals("asdasd", user.getUserId());
  }

  @Test
  void setUserId() {
    user.setUserId("asdasd");
    assertEquals("asdasd", user.getUserId());
  }

  @Test
  void setListGroup() {
    Set<Group> groups = new HashSet<>();
    user.setListGroup(groups);
    assertEquals(0, user.getListGroupSize());
  }

  @Test
  void getListGroupSize() {
    Set<Group> groups = new HashSet<>();
    user.setListGroup(groups);
    assertEquals(0, user.getListGroupSize());
  }
}