package com.remi.core.model.builder;

import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnnouncementBuilderTest {
  private AnnouncementBuilder announcementBuilder;

  @BeforeEach
  void setUp() {
    announcementBuilder = new AnnouncementBuilder();
  }

  @Test
  void addGroupTest() {
    Group group = new GroupBuilder().createGroup();
    User user = new UserBuilder().createUser();
    group.setGroupMember(new HashSet<>());
    user.setListGroup(new HashSet<>());
    user.addGroup(group);

    assertEquals(1, user.getListGroupSize());
  }

  @Test
  void removeGroupTest() {
    Group group = new GroupBuilder().createGroup();
    User user = new UserBuilder().createUser();
    group.setGroupMember(new HashSet<>());
    user.setListGroup(new HashSet<>());
    user.addGroup(group);

    user.removeGroup(group);
    assertEquals(0, user.getListGroupSize());
  }

  @Test
  void setAnnouncementId() {
    announcementBuilder.setAnnouncementId("asdasd");
    Announcement announcement = announcementBuilder.createAnnouncement();
    assertEquals("asdasd", announcement.getAnnouncementId());
  }

  @Test
  void setTitle() {
    announcementBuilder.setTitle("asdasd");
    Announcement announcement = announcementBuilder.createAnnouncement();
    assertEquals("asdasd", announcement.getTitle());
  }

  @Test
  void setDescription() {
    announcementBuilder.setDescription("asdasd");
    Announcement announcement = announcementBuilder.createAnnouncement();
    assertEquals("asdasd", announcement.getDescription());
  }

  @Test
  void setGroup() {
    Group group = new Group(1, "asd", new User("asd", "asd", null), "asd" );
    announcementBuilder.setGroup(group);
    Announcement announcement = announcementBuilder.createAnnouncement();
    assertEquals(group, announcement.getGroup());
  }

  @Test
  void createAnnouncement() {
    announcementBuilder.setDescription("asdasd");
    Announcement announcement = announcementBuilder.createAnnouncement();
    assertEquals("asdasd", announcement.getDescription());
  }

}
