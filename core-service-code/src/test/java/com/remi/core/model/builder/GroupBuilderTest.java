package com.remi.core.model.builder;

import com.remi.core.model.Group;
import com.remi.core.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GroupBuilderTest {
  private UserBuilder userBuilder;
  private GroupBuilder groupBuilder;

  @BeforeEach
  void setUp() {
    userBuilder = new UserBuilder();
    groupBuilder = new GroupBuilder();
  }

  @Test
  void setGroupName() {
    groupBuilder.setGroupName("asdasd");
    Group group = groupBuilder.createGroup();
    assertEquals("asdasd", group.getGroupName());
  }

  @Test
  void setGroupId() {
    groupBuilder.setGroupId(1);
    Group group = groupBuilder.createGroup();
    assertEquals(1, group.getGroupId());
  }

  @Test
  void setDescription() {
    groupBuilder.setDescription("asdasd");
    Group group = groupBuilder.createGroup();
    assertEquals("asdasd", group.getDescription());
  }

  @Test
  void setUser() {
    User user = userBuilder.createUser();
    groupBuilder.setUser(user);
    Group group = groupBuilder.createGroup();
    assertEquals(user, group.getUser());
  }

  @Test
  void setGroupMember() {
    Set<User> userSet = new HashSet<>();
    User user = userBuilder.createUser();
    userSet.add(user);
    groupBuilder.setGroupMember(userSet);
    Group group = groupBuilder.createGroup();
    assertEquals(1, group.getGroupMemberSize());
  }

}
