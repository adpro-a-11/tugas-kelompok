package com.remi.core.model.builder;

import com.remi.core.model.Group;
import com.remi.core.model.Reminder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReminderBuilderTest {
  private ReminderBuilder reminderBuilder;
  private GroupBuilder groupBuilder;

  @BeforeEach
  void setUp() {
    reminderBuilder = new ReminderBuilder();
    groupBuilder = new GroupBuilder();
  }

  @Test
  void setReminderId() {
    reminderBuilder.setReminderId("asd");
    Reminder reminder = reminderBuilder.createReminder();
    assertEquals("asd", reminder.getReminderId());
  }

  @Test
  void setTitle() {
    reminderBuilder.setTitle("asd");
    Reminder reminder = reminderBuilder.createReminder();
    assertEquals("asd", reminder.getTitle());
  }

  @Test
  void setGroup() {
    Group group = groupBuilder.createGroup();
    reminderBuilder.setGroup(group);
    Reminder reminder = reminderBuilder.createReminder();
    assertEquals(group, reminder.getGroup());
  }

  @Test
  void setDescription() {
    reminderBuilder.setDescription("asd");
    Reminder reminder = reminderBuilder.createReminder();
    assertEquals("asd", reminder.getDescription());
  }

  @Test
  void setDateTime() {
    Date date = new Date();
    reminderBuilder.setDateTime(date);
    Reminder reminder = reminderBuilder.createReminder();
    assertEquals(date, reminder.getDateTime());
  }

}