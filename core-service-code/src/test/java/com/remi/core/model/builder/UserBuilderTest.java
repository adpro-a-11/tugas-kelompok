package com.remi.core.model.builder;

import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserBuilderTest {
  private UserBuilder userBuilder;
  private GroupBuilder groupBuilder;

  @BeforeEach
  void setUp() {
    userBuilder = new UserBuilder();
    groupBuilder = new GroupBuilder();
  }

  @Test
  void setUserId() {
    userBuilder.setUserId("asdasd");
    User user = userBuilder.createUser();
    assertEquals("asdasd", user.getUserId());
  }

  @Test
  void setTitle() {
    userBuilder.setName("asdasd");
    User user = userBuilder.createUser();
    assertEquals("asdasd", user.getName());
  }

  @Test
  void setListGroup() {
    Set<Group> groupSet = new HashSet<>();
    Group group = groupBuilder.createGroup();
    groupSet.add(group);
    userBuilder.setListGroup(groupSet);
    User user = userBuilder.createUser();
    assertEquals(1, user.getListGroupSize());
  }

}
