package com.remi.core.model.repository;

import com.remi.core.model.Group;
import com.remi.core.model.User;

import java.util.HashSet;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GroupRepositoryTest {
  @Autowired
  private DataSource dataSource;
  @Autowired
  private JdbcTemplate jdbcTemplate;
  @Autowired
  private EntityManager entityManager;
  @Autowired
  private GroupRepository groupRepository;
  @Autowired
  private UserRepository userRepository;

  @Test
  void getGroupByUser(){
    User user = new User();
    user.setListGroup(new HashSet<>());
    user.setName("asdasd");
    user.setUserId("asdasdasdsadasd");

    userRepository.save(user);
    user = userRepository.getOne("asdasdasdsadasd");

    Group group = new Group();
    group.setDescription("asd");
    group.setGroupName("group1");
    group.setUser(user);
    group.setGroupMember(new HashSet<>());
    groupRepository.save(group);

    Group group1 = groupRepository.getGroupByUser(user);
    assertTrue(groupRepository.existsGroupsByUser(user));
    assertThat(group1).isNotNull();
    assertEquals("group1",group1.getGroupName());
  }
}
