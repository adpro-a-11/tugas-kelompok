package com.remi.core.proxy.group;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.repository.AnnouncementRepository;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.service.announcement.AnnouncementService;
import com.remi.core.service.group.GroupService;
import com.remi.core.service.reminder.ReminderService;
import com.remi.core.utils.TextMessageData;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class GroupProxyImplTest {

  @Mock
  private ReminderService reminderService;

  @Mock
  private UserRepository userRepository;

  @Mock
  private GroupService groupService;

  @Mock
  private AnnouncementService announcementService;

  @Mock
  private MessagingFeign messagingFeign;

  @Mock
  private GroupRepository groupRepository;

  @Mock
  private AnnouncementRepository announcementRepository;

  @InjectMocks
  private GroupProxyImpl groupProxy;

  private User user1;

  private User user;

  private Group group;

  private TextMessageData textMessageData;

  private Announcement announcement;

  @BeforeEach
  void setUp(){
    user = new User();
    user.setUserId("userId");
    user.setName("user");
    user.setListGroup(new HashSet<>());

//    userRepository.save(user);

    user1 = new User();
    user1.setListGroup(new HashSet<>());
    user1.setName("user1");
    user1.setUserId("userId1");

//    userRepository.save(user1);

    Set<User> userSet = new HashSet<>();
    userSet.add(user);
    group = new Group();
    group.setGroupMember(userSet);
    group.setUser(user);
    group.setGroupName("group1");
    group.setDescription("desc");
    group.setGroupId(10);

    user.addGroup(group);

    textMessageData = new TextMessageData();
    textMessageData.setUserId("asd");
    textMessageData.setReplyToken("asd");

    announcement = new Announcement();
    announcement.setAnnouncementId("10-title");
    announcement.setDescription("desc");
    announcement.setTitle("title");
    announcement.setGroup(group);



//    groupRepository.save(group);
//    group = groupRepository.getGroupByUser(user);
  }

  @Test
  void checkCommandCreateGroup2Length() {
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/create_group nama");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce()).createGroup(
            user,
            textMessageData.getReplyToken(),
            "nama");
  }
  @Test
  void checkCommandCreateGroup3Length() {
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/create_group nama - desc");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce()).createGroup(
            user,
            textMessageData.getReplyToken(),
            "nama",
            "desc");
  }
  @Test
  void checkCommandCreateGroupWrongFormat() {
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/create_group");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeast(1))
        .sendReplyMessage(
                Map.of(
                        textMessageData.getReplyToken(), "Wrong Format"
                )
        );
  }
  @Test
  void checkCommandCreateGroupAlreadyAdmin() {
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/create_group nama");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeast(1))
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "You already own a group"
                    )
            );
  }
  @Test
  void checkCommandUnknownCommand() {
    textMessageData.setMessage("/asdasdsad");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeast(1))
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Unknown Command"
                    )
            );
  }
  @Test
  void checkCommandNotGroupAdmin() {
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(false);
    textMessageData.setMessage("/delete_group");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeast(1))
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "You don't own a group"
                    )
            );
  }
  @Test
  void checkCommandDetailGroupAdmin(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/detail_group");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeast(1))
            .detailGroup(user,textMessageData.getReplyToken());
  }
  @Test
  void checkCommandDetailGroupUser(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/detail_group 10");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeast(1))
            .detailGroup(user,textMessageData.getReplyToken(),group);
  }
  @Test
  void checkCommandDetailGroupUserGroupIdNotNumber(){
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/detail_group asd");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Group id must be a number"
                    )
            );
  }
  @Test
  void checkCommandDetailGroupUserGroupIdWrongFormat(){
    textMessageData.setMessage("/detail_group 12 13");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Wrong Format"
                    )
            );
  }
  @Test
  void checkCommandNotGroupMember(){
    when(userRepository.getOne("asd")).thenReturn(user1);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/detail_group 10");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "You're not member of that group"
                    )
            );
  }
  @Test
  void checkCommandAlreadyGroupMember(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/add_group 10");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Already be a member in the group"
                    )
            );
  }
  @Test
  void checkCommandUserParamNotValid(){
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/add_group");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Wrong Format"
                    )
            );
  }
  @Test
  void checkCommandUserAddGroup(){
    when(userRepository.getOne("asd")).thenReturn(user1);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/add_group 10");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .addGroup(user1,textMessageData.getReplyToken(),group);
  }
  @Test
  void checkCommandUpdateGroupName(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group nama 0");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .updateGroup(user,textMessageData.getReplyToken(),"nama",0);
  }
  @Test
  void checkCommandUpdateGroupNameWithSpace(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group nama nama 0");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .updateGroup(user,textMessageData.getReplyToken(),"nama nama",0);
  }
  @Test
  void checkCommandUpdateGroupDesc(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group desc 1");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .updateGroup(user,textMessageData.getReplyToken(),"desc",1);
  }
  @Test
  void checkCommandUpdateGroupDescWithSpace(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group desc dgsdg 1");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .updateGroup(user,textMessageData.getReplyToken(),"desc dgsdg",1);
  }
  @Test
  void checkCommandUpdateGroupPartCodeNotNumber(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group nama asd");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Part code must be a number"
                    )
            );
  }
  @Test
  void checkCommandUpdateGroupWrongPartCode(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group nama 2");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Wrong part code"
                    )
            );
  }
  @Test
  void checkCommandUpdateGroupWrongFormat(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/update_group nama");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            textMessageData.getReplyToken(), "Wrong Format"
                    )
            );
  }
  @Test
  void checkCommandListGroup(){
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/list_group");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .listGroup(user,textMessageData.getReplyToken());
  }
  @Test
  void checkCommandRemoveGroup(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/remove_group 10");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .removeGroup(user,textMessageData.getReplyToken(),group);
  }
  @Test
  void checkCommandDeleteGroup(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/delete_group");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .deleteGroup(user,textMessageData.getReplyToken());
  }
  @Test
  void checkCommandListMemberGroup(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/list_group_member");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .listGroupMember(user,textMessageData.getReplyToken());
  }
  @Test
  void checkCommandCountGroupReminder(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/count_group_reminder");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .countGroupReminder(user,textMessageData.getReplyToken());
  }
  @Test
  void checkCommandCountGroupAnnouncement(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/count_group_announcement");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(groupService,atLeastOnce())
            .countGroupAnnouncement(user,textMessageData.getReplyToken());
  }

  @Test
  void checkCommandCreateAnnouncementWithDesc(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/announce_with_desc - Title - Desc");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(announcementService,atLeastOnce())
            .createAnnouncement(user, textMessageData.getReplyToken(), true,
                    "Title", "Desc");
  }

  @Test
  void checkCommandCreateAnnouncementWithoutDesc(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/announce_without_desc - Title");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(announcementService,atLeastOnce())
            .createAnnouncement(user, textMessageData.getReplyToken(), false,
                    "Title", " ");
  }

  @Test
  void checkCommandCreatedAnnouncementList(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/created_announcement_list");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(announcementService,atLeastOnce())
            .listOfAnnouncementsByAdmin(textMessageData);
  }

  @Test
  void checkCommandSeeAnnouncement(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/see_announcement 10 title title");
    groupProxy.checkCommand(textMessageData);
    verify(announcementService,atLeastOnce())
            .seeAnnouncement(textMessageData.getReplyToken(),10, "title title");
  }

  @Test
  void checkCommandSeeAnnouncementWrongFormat(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/see_announcement 10");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeast(1))
      .sendReplyMessage(
        Map.of(
          textMessageData.getReplyToken(), "Wrong Format"
        )
      );
  }

  @Test
  void checkCommandAvailableAnnouncement(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.getOne(10)).thenReturn(group);
    textMessageData.setMessage("/available_announcement 10");
    groupProxy.checkCommand(textMessageData);
    verify(announcementService,atLeastOnce())
            .listOfAnnouncementsByUser(textMessageData,"10");
  }

  @Test
  void createReminderWithDesc(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderService.createReminderId(group,"test")).thenReturn("10-test");
    textMessageData.setMessage("/reminder_with_desc - test - test - 12-12-2012 12:12 - T-1");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
      .createReminderByReminderId(
        "10-test",
        user,
        textMessageData.getReplyToken(),
        true,
        "test",
        "12-12-2012 12:12",
        "T-1");
  }

  @Test
  void createReminderWithDescWrongFormat(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderService.createReminderId(group,"test")).thenReturn("10-test");
    textMessageData.setMessage("/reminder_with_desc - test - test - 12-12-2012 12:12");
    groupProxy.checkCommand(textMessageData);
    Mockito.verify(messagingFeign,atLeast(1))
      .sendReplyMessage(
        Map.of(
          textMessageData.getReplyToken(), "Wrong Format"
        )
      );
  }

  @Test
  void createReminderWithoutDesc(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderService.createReminderId(group,"test")).thenReturn("10-test");
    textMessageData.setMessage("/reminder_without_desc - test - 12-12-2012 12:12 - T-1");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
      .createReminderByReminderId(
        "10-test",
        user,
        textMessageData.getReplyToken(),
        false,
        "",
        "12-12-2012 12:12",
        "T-1");
  }

  @Test
  void reminderDelete(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderService.createReminderId(group,"test")).thenReturn("10-test");
    textMessageData.setMessage("/reminder_delete - test");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
        .deleteReminder(textMessageData.getReplyToken(), "10-test");
  }

  @Test
  void reminderChangeDate(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderService.createReminderId(group,"test")).thenReturn("10-test");
    textMessageData.setMessage("/reminder_change_date - test - 12-12-2012 12:12");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
      .changeReminderDate(textMessageData.getReplyToken(), "10-test","12-12-2012 12:12");
  }
  @Test
  void reminderChangeDesc(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderService.createReminderId(group,"test")).thenReturn("10-test");
    textMessageData.setMessage("/reminder_change_description - test - testing");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
      .changeReminderDescription(textMessageData.getReplyToken(), "10-test","testing");
  }
  @Test
  void reminderListAllGroup(){
    when(userRepository.getOne("asd")).thenReturn(user);
    when(groupRepository.existsGroupsByUser(user)).thenReturn(true);
    textMessageData.setMessage("/reminder_list_all_group");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
      .listReminderByAdmin(textMessageData);
  }
  @Test
  void reminderListAll(){
    when(userRepository.getOne("asd")).thenReturn(user);
    textMessageData.setMessage("/reminder_list_all");
    groupProxy.checkCommand(textMessageData);
    verify(reminderService,atLeastOnce())
      .listReminderByUser(textMessageData,"asd");
  }




}