package com.remi.core.proxy.user;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.proxy.group.GroupProxy;
import com.remi.core.proxy.group.GroupProxyImpl;
import com.remi.core.service.group.GroupService;
import com.remi.core.utils.TextMessageData;

import java.util.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class UserProxyImplTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private MessagingFeign messagingFeign;

  @Mock
  private GroupProxy groupProxy;

  @InjectMocks
  private UserProxyImpl userProxy;

  private User user;
  private User userRegistered;
  private Group group;
  private Announcement announcement;
  private List<Announcement> announcementList;
  private TextMessageData textMessageData;
  private TextMessageData textMessageDataRegistered;

  @BeforeEach
  void setUp(){
    user = new User();
    user.setUserId("userId");
    user.setName("user");

    userRegistered = new User();
    userRegistered.setUserId("userRegisteredId");
    userRegistered.setName("userRegistered");

    textMessageData = new TextMessageData();
    textMessageData.setReplyToken("replyTokenData");
    textMessageData.setUserId("userId");

    textMessageDataRegistered = new TextMessageData();
    textMessageDataRegistered.setReplyToken("replyTokenDataRegistered");
    textMessageDataRegistered.setUserId("userRegisteredId");

    userRepository.save(userRegistered);
  }

  @Test
  void checkMessageHelp(){
    textMessageData.setMessage("/help");
    userProxy.checkMessage(textMessageData);
    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
            Map.of(
                    "replyTokenData",
                    "Please register first with this format: "
                            + "\n /register [name] \n ex: /register Remi"
            )
    );
  }
  @Test
  void checkMessageHelpRegistered(){
    textMessageData.setMessage("/help");
    when(userRepository.existsById("userId")).thenReturn(true);
    userProxy.checkMessage(textMessageData);
    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
            Map.of(
                    "replyTokenData",
              "List of available commands with formats: \n \n"

                + "\nAdmin: \n "
                + "\n/create_group [name] - [desc:optional]\n"
                + "\n/update_group [value] [part-code]\n"
                + "note: replace title(part-code = 0) or desc(part-code = 1) with value\n"
                + "\n/delete_group\n"
                + "\n/list_group_member\n"
                + "\n/detail_group\n"
                + "\n/count_group_reminder\n"
                + "\n/count_group_announcement\n"
                + "\n/announce_with_desc - [title] - [desc]\n"
                + "\n/announce_without_desc - [title]\n"
                + "\n/created_announcement_list\n\n"
                + "note: \n"
                + "date format: dd-MM-yyyy HH:mm\n"
                + "activation settings : T-3 (3 minute before), T-5 (5 minute before) , H-7 (7 days before)\n\n"
                + "\n/reminder_with_desc - [title] - [desc] - [date] - [activation setting]\n"
                + "\n/reminder_without_desc - [title] - [date] - [activation setting]\n"
                + "\n/reminder_delete - [title]\n"
                + "\n/reminder_change_date - [title] - [date]\n"
                + "\n/reminder_change_description - [title] - [description]\n"
                + "\n/reminder_list_all_group\n \n"

                + "\nUser: \n "
                + "\n/list_group\n"
                + "\n/add_group [group-id]\n"
                + "\n/remove_group [group-id]\n"
                + "\n/detail_group [group-id]\n"
                + "\n/see_announcement [group-id] [title]\n"
                + "\n/available_announcement [group-id]\n"
                + "\n/reminder_list_all"
            )
    );
  }
  @Test
  void checkMessageHelpRegisteredGroupCommand(){
    textMessageData.setMessage("/list_group");
    when(userRepository.existsById("userId")).thenReturn(true);
    userProxy.checkMessage(textMessageData);
    verify(groupProxy,atLeastOnce()).checkCommand(textMessageData);
  }
  @Test
  void checkMessageHelpRegisteredUknownCommand(){
    textMessageData.setMessage("/list");
    when(userRepository.existsById("userId")).thenReturn(true);
    userProxy.checkMessage(textMessageData);
    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
            Map.of(
                    "replyTokenData",
                    "Please type /help for instructions"
            )
    );
  }

  @Test
  void checkMessageRegister(){
    textMessageData.setMessage("/register adpro");
    userProxy.checkMessage(textMessageData);
    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
            Map.of(
                    "replyTokenData",
                    "Welcome, adpro!"
            )
    );
  }

  @Test
  void checkMessageError(){
    textMessageData.setMessage("sfddssf");
    userProxy.checkMessage(textMessageData);
    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
            Map.of(
                    "replyTokenData",
                    "Please type /help for instructions"
            )
    );
  }

//  @Test
//  void checkMessageRegisteredHelp(){
//    textMessageDataRegistered.setMessage("/help");
//    userProxy.checkMessage(textMessageDataRegistered);
//    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
//            Map.of(
//                    "replyToken",
//                    "List of available commands:"
//            )
//    );
//  }
//
//  @Test
//  void checkMessageRegisteredError(){
//    textMessageDataRegistered.setMessage("asdasd");
//    userProxy.checkMessage(textMessageDataRegistered);
//    verify(messagingFeign,atLeastOnce()).sendReplyMessage(
//            Map.of(
//                    "replyTokenDataRegistered",
//                    "Please type /help for instructions"
//            )
//    );
//  }


}