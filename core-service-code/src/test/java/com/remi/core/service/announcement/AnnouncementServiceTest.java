package com.remi.core.service.announcement;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Announcement;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.repository.AnnouncementRepository;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.UserRepository;

import java.util.*;

import com.remi.core.utils.TextMessageData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AnnouncementServiceTest {
  @Mock
  private MessagingFeign messagingFeign;
  @Mock
  private GroupRepository groupRepository;
  @Mock
  private UserRepository userRepository;
  @Mock
  private AnnouncementRepository announcementRepository;
  @InjectMocks
  private AnnouncementServiceImpl announcementService;

  private User user;
  private Group group;
  private Announcement announcement;
  private List<Announcement> announcementList;
  private TextMessageData textMessageData;

  @BeforeEach
  void setUp() {
    user = new User();
    user.setUserId("userId");
    user.setName("user");
    user.setListGroup(new HashSet<>());

    Set<User> userSet = new HashSet<>();
    userSet.add(user);
    group = new Group();
    group.setGroupMember(userSet);
    group.setUser(user);
    group.setGroupName("group1");
    group.setDescription("desc");
    group.setGroupId(10);
    user.addGroup(group);

    announcement = new Announcement();
    announcement.setAnnouncementId("10-title");
    announcement.setDescription("desc");
    announcement.setTitle("title");
    announcement.setGroup(group);

    announcementList = new ArrayList<>();
    announcementList.add(announcement);

    textMessageData = new TextMessageData();
    textMessageData.setReplyToken("replyTokenData");
    textMessageData.setUserId("userId");
  }

  @Test
  void createAnnouncementWithoutDesc() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    announcementService.createAnnouncement(user, "replyToken",
            false, "title", "desc" );
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendPushMessage(
                    Map.of(
                            "userId","Title: title"
                    )
            );

  }

  @Test
  void createAnnouncementWithDesc() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    announcementService.createAnnouncement(user, "replyToken",
            true, "title", "desc" );
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendPushMessage(
                    Map.of(
                            "userId","Title: title \n Description: desc"
                    )
            );

  }

  @Test
  void seeAnnouncementTest() {
    when(announcementRepository.getOne("10-title")).thenReturn(announcement);
    announcementService.seeAnnouncement("replyToken", group.getGroupId(), "title");
    verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "replyToken","Title: title \n Description: desc"
                    )
            );

  }

  @Test
  void listOfAnnouncementsAdminTest() {
    Mockito.when(userRepository.getOne(textMessageData.getUserId())).thenReturn(user);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(announcementRepository.findAnnouncementsByGroup(group)).thenReturn(announcementList);
    announcementService.listOfAnnouncementsByAdmin(textMessageData);
    verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "replyTokenData","List of Announcements:\n1. title - ID: 10-title\n"
                    )
            );

  }

  @Test
  void listOfAnnouncementsUserTest() {
    when(groupRepository.getOne(group.getGroupId())).thenReturn(group);
    when(announcementRepository.findAnnouncementsByGroup(group)).thenReturn(announcementList);
    announcementService.listOfAnnouncementsByUser(textMessageData, String.valueOf(group.getGroupId()));
    verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "replyTokenData","List of Announcements:\n1. title - ID: 10-title\n"
                    )
            );

  }


}
