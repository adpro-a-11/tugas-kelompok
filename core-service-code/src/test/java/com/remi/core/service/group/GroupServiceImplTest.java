package com.remi.core.service.group;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.Group;
import com.remi.core.model.User;
import com.remi.core.model.repository.AnnouncementRepository;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.model.repository.ReminderRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.utils.DateUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GroupServiceImplTest {

  @Mock
  private MessagingFeign messagingFeign;
  @Mock
  private GroupRepository groupRepository;
  @Mock
  private ReminderRepository reminderRepository;
  @Mock
  private AnnouncementRepository announcementRepository;
  @InjectMocks
  private GroupServiceImpl groupService;

  private User user1;

  private User user;

  private Group group;

  @BeforeEach
  void setUp(){
    user = new User();
    user.setUserId("userId");
    user.setName("user");
    user.setListGroup(new HashSet<>());

//    userRepository.save(user);

    user1 = new User();
    user1.setListGroup(new HashSet<>());
    user1.setName("user1");
    user1.setUserId("userId1");

//    userRepository.save(user1);

    Set<User> userSet = new HashSet<>();
    userSet.add(user);
    group = new Group();
    group.setGroupMember(userSet);
    group.setUser(user);
    group.setGroupName("group1");
    group.setDescription("desc");
    group.setGroupId(10);

    user.addGroup(group);

//    groupRepository.save(group);
//    group = groupRepository.getGroupByUser(user);


  }

  @Test
  void createGroup() {
    groupService.createGroup(user1,"asdasd","something");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                        "asdasd","Group something created"
                    )
            );

  }

  @Test
  void createGroupWithDesc(){
    groupService.createGroup(user1,"asdasd","something","something");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasd","Group something created"
                    )
            );
  }

  @Test
  void deleteGroup() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    groupService.deleteGroup(user,"asdasd");
    Mockito.verify(groupRepository,atLeastOnce())
            .deleteById(group.getGroupId());
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasd","Success delete group group1"
                    )
            );

  }

  @Test
  void updateGroupName() {
    group = spy(group);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    groupService.updateGroup(user,"asdasd","asdasdsad",0);
    Mockito.verify(group,atLeastOnce())
            .setGroupName("asdasdsad");
    Mockito.verify(groupRepository,atLeastOnce())
            .save(group);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasd","Change group name to asdasdsad"
                    )
            );
  }

  @Test
  void updateGroupDesc() {
    group = spy(group);
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    groupService.updateGroup(user,"asdasd","asdasdsad",1);
    Mockito.verify(group,atLeastOnce())
            .setDescription("asdasdsad");
    Mockito.verify(groupRepository,atLeastOnce())
            .save(group);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasd","Change group description to asdasdsad"
                    )
            );
  }

  @Test
  void addGroup() {
    group = spy(group);
    groupService.addGroup(user1,"asdasd",group);
    Mockito.verify(group,atLeastOnce())
            .addGroupMember(user1);
    Mockito.verify(groupRepository,atLeastOnce())
            .save(group);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasd","Added to group group1"
                    )
            );
  }

  @Test
  void removeGroup() {
    group = spy(group);
    groupService.removeGroup(user,"asdasdas",group);
    Mockito.verify(group,atLeastOnce())
            .removeGroupMember(user);
    Mockito.verify(groupRepository,atLeastOnce())
            .save(group);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasdas","Removed from group group1"
                    )
            );
  }

  @Test
  void detailGroup() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderRepository.countRemindersByGroup(group)).thenReturn(0);
    DateUtils dateUtils = DateUtils.getInstance();
    when(reminderRepository.countRemindersByGroupAndDateTimeGreaterThan(group,dateUtils.normalizeDate(new Date()))).thenReturn(0);
    when(announcementRepository.countAnnouncementsByGroup(group)).thenReturn(0);
    groupService.detailGroup(user,"asdasd");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                           "asdasd",  String.format(
                                    "Group %s - Id %d\n" +
                                            "Description :\n" +
                                            "%s\n" +
                                            "Total Member : %d\n" +
                                            "Total Reminder : %d\n" +
                                            "Active Reminder : %d\n" +
                                            "Total Announcement : %d\n",
                                    group.getGroupName(),
                                    group.getGroupId(),
                                    group.getDescription(),
                                    group.getGroupMemberSize(),
                                    0,
                                    0,
                                    0)
                    )
            );
  }

  @Test
  void testDetailGroup() {
    DateUtils dateUtils = DateUtils.getInstance();
    when(reminderRepository.countRemindersByGroupAndDateTimeGreaterThan(group,dateUtils.normalizeDate(new Date()))).thenReturn(0);
    groupService.detailGroup(user,"asdasd",group);
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasd",   String.format(
                                    "Group %s - Id %d\n" +
                                            "Description :\n" +
                                            "%s\n" +
                                            "Member : %d\n" +
                                            "Active Reminder : %d",
                                    group.getGroupName(),
                                    group.getGroupId(),
                                    group.getDescription(),
                                    group.getGroupMemberSize(),0)
                    )
            );

  }

  @Test
  void listGroupMember() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    groupService.listGroupMember(user,"asdasd");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                        "asdasd","Group Member :\n1. user\n"
                    )
            );
  }

  @Test
  void countGroupReminder() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(reminderRepository.countRemindersByGroup(group)).thenReturn(0);
    DateUtils dateUtils = DateUtils.getInstance();
    when(reminderRepository.countRemindersByGroupAndDateTimeGreaterThan(group,dateUtils.normalizeDate(new Date()))).thenReturn(0);
    groupService.countGroupReminder(user,"asdasd");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                        "asdasd",String.format(
                                    "Group %s\n" +
                                            "Active reminder : %d\n" +
                                            "Total Reminder : %d",
                                    group.getGroupName(),
                                    0,
                                    0
                            )
                    )
            );

  }

  @Test
  void countGroupAnnouncement() {
    when(groupRepository.getGroupByUser(user)).thenReturn(group);
    when(announcementRepository.countAnnouncementsByGroup(group)).thenReturn(0);
    groupService.countGroupAnnouncement(user,"asdasdsad");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasdsad",String.format(
                                    "Group %s\n" +
                                            "Total Announcement : %d",
                                    group.getGroupName(),
                                    0
                            )
                    )
            );
  }
  @Test
  void listGroup() {
    groupService.listGroup(user,"asdasdsad");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasdsad","List Group :\n1. group1 - Id : 10\n"
                    )
            );
  }
  @Test
  void dontHaveAGroup() {
    groupService.listGroup(user1,"asdasdsad");
    Mockito.verify(messagingFeign,atLeastOnce())
            .sendReplyMessage(
                    Map.of(
                            "asdasdsad","You don't join to any group"
                    )
            );
  }
}