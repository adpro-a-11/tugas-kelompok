package com.remi.core.service.reminder;

import com.remi.core.feign.MessagingFeign;
import com.remi.core.model.User;
import com.remi.core.model.Group;
import com.remi.core.model.Reminder;
import com.remi.core.model.repository.ReminderRepository;
import com.remi.core.model.repository.UserRepository;
import com.remi.core.model.repository.GroupRepository;
import com.remi.core.utils.TextMessageData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReminderServiceTest {
		@Mock
		private ReminderRepository reminderRepository;
		@Mock
		private GroupRepository groupRepository;
		@Mock
		private UserRepository userRepository;

		@Mock
		private MessagingFeign messagingFeign;

		@InjectMocks
		private ReminderServiceImpl reminderService = spy(
				new ReminderServiceImpl(reminderRepository,
						userRepository, groupRepository, messagingFeign));

		private User user;
		private Group group;
		private Date date;
		private Reminder reminder;
		private List<Reminder> reminderList;
		private TextMessageData textMessageData;

		@BeforeEach
		void setUp() {
				user = new User();
				user.setUserId("userId");
				user.setName("user");
				user.setListGroup(new HashSet<>());

				Set<User> userSet = new HashSet<>();
				userSet.add(user);
				group = new Group();
				group.setGroupMember(userSet);
				group.setUser(user);
				group.setGroupName("group1");
				group.setDescription("desc");
				group.setGroupId(10);
				user.addGroup(group);

				date = new Date();
				reminder = new Reminder();
				reminder.setReminderId("10-title");
				reminder.setDescription("desc");
				reminder.setTitle("title");
				reminder.setDateTime(date);
				reminder.setActivationSetting(new ArrayList<>());
				reminder.setGroup(group);

				reminderList = List.of(reminder);

				textMessageData = new TextMessageData();
				textMessageData.setReplyToken("replyTokenData");
				textMessageData.setUserId("userId");
		}
		@Test
		void createReminderId(){
				assertEquals(reminderService.createReminderId(group,"test"),"10-test");
		}
		@Test
		void createReminderWithDesc(){
				when(groupRepository.getGroupByUser(user)).thenReturn(group);
				reminderService.createReminderByReminderId(
						"10-test",
						user,
						textMessageData.getReplyToken(),
						true,
						"test",
						"12-12-2012 12:12",
						"T-1"
				);
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										String.format("Reminder with Title: %s and Description: %s" +
												" created on %s", "test", "test","12-12-2012 12:12" )
								)
						);
		}
		@Test
		void createReminderWithoutDesc(){
				when(groupRepository.getGroupByUser(user)).thenReturn(group);
				reminderService.createReminderByReminderId(
						"10-test",
						user,
						textMessageData.getReplyToken(),
						false,
						"test",
						"12-12-2012 12:12",
						"T-1"
				);
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										String.format("Reminder with Title: %s created on %s", "test","12-12-2012 12:12" )
								)
						);
		}
		@Test
		void createReminderWithDescWrongDateFormat(){
				when(groupRepository.getGroupByUser(user)).thenReturn(group);
				reminderService.createReminderByReminderId(
						"10-test",
						user,
						textMessageData.getReplyToken(),
						true,
						"test",
						"12-12-2012 12:asd",
						"T-1"
				);
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										"Wrong Date Format must be dd-MM-yyyy HH:mm"
								)
						);
		}
		@Test
		void changeReminderDate() throws ParseException {
				when(reminderRepository.getOne("10-test")).thenReturn(reminder);
				Date oldDate = reminder.getDateTime();
				reminderService.changeReminderDate(
						textMessageData.getReplyToken(),
						"10-test",
						"12-12-2012 12:12"
				);
				Date date = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("12-12-2012 12:12");
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										String.format("Reminder with " +
														"title: %s has been changed from %s to %s",
												reminder.getTitle(), oldDate.toString(),
												"12-12-2012 12:12")
								)
						);
		}
		@Test
		void changeReminderDateWrongFormat() throws ParseException {
				when(reminderRepository.getOne("10-test")).thenReturn(reminder);
				Date oldDate = reminder.getDateTime();
				reminderService.changeReminderDate(
						textMessageData.getReplyToken(),
						"10-test",
						"12-12-2012 12:asdasd"
				);
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										"Wrong Date Format must be dd-MM-yyyy HH:mm"
								)
						);
		}
		@Test
		void changeReminderDescription(){
				when(reminderRepository.getOne("10-test")).thenReturn(reminder);
				String pastDesc = reminder.getDescription();
				reminderService.changeReminderDescription(
						textMessageData.getReplyToken(),
						"10-test",
						"test"
				);
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										String.format("Reminder with title: %s has been changed " +
												"from %s to %s", reminder.getTitle(), pastDesc, "test")
								)
						);
		}
		@Test
		void deleteReminder(){
				when(reminderRepository.getOne("10-test")).thenReturn(reminder);
				reminderService.deleteReminder(textMessageData.getReplyToken(), "10-test");
				verify(reminderRepository,atLeastOnce())
						.deleteById("10-test");
				Mockito.verify(messagingFeign,atLeast(1))
						.sendReplyMessage(
								Map.of(
										textMessageData.getReplyToken(),
										"Reminder" + reminder.getTitle() + " is successfully deleted"
								)
						);
		}

		@Test
		void listReminderByAdmin(){
				when(userRepository.getOne(textMessageData.getUserId())).thenReturn(user);
				when(groupRepository.getGroupByUser(user)).thenReturn(group);
				when(reminderRepository.findRemindersByGroup(group)).thenReturn(reminderList);
				reminderService.listReminderByAdmin(textMessageData);
				Mockito.verify(messagingFeign,atLeast(1))
					.sendReplyMessage(
						Map.of(
							textMessageData.getReplyToken(),
							"List of Reminders in Group "
                + group.getGroupName() + ":\n"
								+ String.format("%d. %s - ID: %s\n",1,
								reminder.getTitle(),reminder.getReminderId())
					)
				);
		}

		@Test
		void listReminderByUser(){
				when(userRepository.getOne("asd")).thenReturn(user);
				when(reminderRepository.findRemindersByGroup(group)).thenReturn(reminderList);
				reminderService.listReminderByUser(textMessageData,"asd");
				Mockito.verify(messagingFeign,atLeast(1))
					.sendReplyMessage(
						Map.of(
							textMessageData.getReplyToken(),
							"All Your Reminders\n\n"
                + "List of Reminders in Group "
								+ group.getGroupName() + ":\n"
								+ String.format("%d. %s - ID: %s\n",1,
								reminder.getTitle(),reminder.getReminderId())
								+"\n"
					)
				);
		}
}
