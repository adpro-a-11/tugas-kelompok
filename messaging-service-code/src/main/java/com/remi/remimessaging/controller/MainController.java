package com.remi.remimessaging.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.remi.remimessaging.feign.CoreFeign;
import com.remi.remimessaging.message.ReminderMessage;
import com.remi.remimessaging.model.Reminder;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

  private final LineMessagingClient lineMessagingClient;

  private final CoreFeign coreFeign;

  private final Logger logger = LoggerFactory.getLogger(MainController.class);

  MainController(LineMessagingClient lineMessagingClient, CoreFeign coreFeign) {
    this.lineMessagingClient = lineMessagingClient;
    this.coreFeign = coreFeign;
  }

  /**
   * controller for push message.
   *
   * @param data messageData
   * @return string
   */
  @PostMapping("/message/push")
  public String sendPushMessage(@RequestBody Map<String, String> data) {
    for (Map.Entry<String, String> entry : data.entrySet()) {
      String userId = entry.getKey();
      String message = entry.getValue();
      TextMessage textMessage = new TextMessage(message);
      PushMessage pushMessage = new PushMessage(userId, textMessage);

      try {
        lineMessagingClient
                .pushMessage(pushMessage)
                .get();
      } catch (Exception e) {
        e.printStackTrace();
        logger.error(String.format("Fail to send message to %s at %s", userId, new Date()));
      }
    }
    return "Done";
  }

  /**
   * controller for reply message.
   *
   * @param data messageData
   * @return string
   */
  @PostMapping("/message/reply")
  public String sendReplyMessage(@RequestBody Map<String, String> data) {
    for (Map.Entry<String, String> entry : data.entrySet()) {
      String replyToken = entry.getKey();
      String message = entry.getValue();
      TextMessage textMessage = new TextMessage(message);
      ReplyMessage pushMessage = new ReplyMessage(replyToken, textMessage);

      try {
        lineMessagingClient
                .replyMessage(pushMessage)
                .get();
      } catch (Exception e) {
        e.printStackTrace();
        logger.error(String.format("Fail to send reply message at %s", new Date()));
      }

    }
    return "Done";
  }

  /**
   * Reminder message controller.
   *
   * @param reminderData      reminderData
   * @param activationSetting activation String
   * @return log message
   */
  @PostMapping("/message/reminder/{activation}")
  public String sendReminderMessage(
          @RequestBody Map<String, Reminder> reminderData,
          @PathVariable("activation") String activationSetting) {
    for (Map.Entry<String, Reminder> entry : reminderData.entrySet()) {
      String userId = entry.getKey();
      Reminder reminder = entry.getValue();
      ReminderMessage reminderMessage =
              new ReminderMessage(activationSetting, reminder);
      FlexMessage flexMessage =
              new FlexMessage(reminder.getTitle(), reminderMessage.getFlexMessage());
      PushMessage pushMessage =
              new PushMessage(userId, flexMessage);

      try {
        lineMessagingClient
                .pushMessage(pushMessage)
                .get();
      } catch (Exception e) {
        e.printStackTrace();
        logger.error(String.format(
                "Fail to send reminder message to %s at %s",
                userId, new Date()));
      }
    }
    return "Done";
  }

}
