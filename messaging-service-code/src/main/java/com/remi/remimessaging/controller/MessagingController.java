package com.remi.remimessaging.controller;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.remi.remimessaging.feign.CoreFeign;
import com.remi.remimessaging.utils.PostBackMessageData;
import com.remi.remimessaging.utils.TextMessageData;
import java.text.ParseException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class MessagingController {

  @Autowired
  private CoreFeign coreFeign;

  /**
   * handle text message event.
   * @param messageEvent text event
   */
  @EventMapping
  public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
    String pesan = messageEvent.getMessage().getText().toLowerCase();
    String token = messageEvent.getReplyToken();
    String userId = messageEvent.getSource().getUserId();

    TextMessageData textMessageData =
            new TextMessageData(userId,token,pesan);

    coreFeign.inputTextMessage(textMessageData);
  }

  /**
   * Handle post back event.
   * @param postbackEvent the event
   * @throws ParseException if date can't be parsed
   */
  @EventMapping
  public void handlePostBackEvent(PostbackEvent postbackEvent) throws ParseException {
    PostbackContent postbackContent = postbackEvent.getPostbackContent();
    String postBackData = postbackContent.getData();
    Map<String, String> postBackParam = postbackContent.getParams();
    String replyToken = postbackEvent.getReplyToken();
    String userId = postbackEvent.getSource().getUserId();

    PostBackMessageData postBackMessageData =
            new PostBackMessageData(userId,replyToken,postBackParam,postBackData);

    coreFeign.inputPostBackMessage(postBackMessageData);
  }
}
