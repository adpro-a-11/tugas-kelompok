package com.remi.remimessaging.feign;

import com.remi.remimessaging.model.Reminder;
import com.remi.remimessaging.model.User;
import com.remi.remimessaging.utils.PostBackMessageData;
import com.remi.remimessaging.utils.TextMessageData;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("remi-core")
public interface CoreFeign {
  @PostMapping("/save/user")
  public String saveUser(@RequestBody User user);

  @PostMapping("/create/reminder/{group_id}/{user_id}")
  public String createReminder(
          @RequestBody Reminder reminder,
          @PathVariable("group_id") String groupId,
          @PathVariable("user_id") String userId);

  @DeleteMapping("/reminder/{group_id}/{reminder_id}}")
  public String deleteReminder(
          @PathVariable("group_id") String groupId,
          @PathVariable("reminder_id") String reminderId,
          @RequestBody String userId);

  @PostMapping("/reminder/{group_id}/list")
  public List<String> listReminder(
          @RequestBody String userId,
          @PathVariable("group_id") String groupId);

  @PostMapping("/message/text")
  public String inputTextMessage(
          @RequestBody TextMessageData data);

  @PostMapping("/message/postback")
  public String inputPostBackMessage(
          @RequestBody PostBackMessageData data);
}
