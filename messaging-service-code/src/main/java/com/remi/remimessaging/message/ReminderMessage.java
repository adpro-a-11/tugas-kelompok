package com.remi.remimessaging.message;

import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexOffsetSize;
import com.remi.remimessaging.model.Group;
import com.remi.remimessaging.model.Reminder;
import com.remi.remimessaging.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;

public class ReminderMessage {
  private final String activationSetting;
  private final Reminder reminder;
  private final DateUtils dateUtils = DateUtils.getInstance();

  public ReminderMessage(String activationSetting, Reminder reminder) {
    this.activationSetting = activationSetting;
    this.reminder = reminder;
  }

  private Box getHeader() {
    String color =
            activationSetting.equals("D")
                    ?
                    "#FF3F1C"
                    :
                    activationSetting.contains("H")
                            ?
                            "#FF8F00"
                            :
                            "#FF6100";

    return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .content(Text.builder()
                    .text(activationSetting.equals("D") ? "TODAY" : activationSetting.toUpperCase())
                    .size(FlexFontSize.XXL)
                    .align(FlexAlign.CENTER)
                    .build())
            .backgroundColor(color)
            .build();
  }

  private Box getBody() {
    Date reminderDate = reminder.getDateTime();
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE\ndd-MM-yyyy");
    String dateString = dateFormat.format(reminderDate);

    SimpleDateFormat timeFormat = new SimpleDateFormat("HH : mm");
    String timeString = timeFormat.format(reminderDate);

    FlexComponent title =
            Text.builder()
                    .text(reminder.getTitle())
                    .size(FlexFontSize.LG)
                    .weight(Text.TextWeight.BOLD)
                    .align(FlexAlign.CENTER)
                    .build();
    FlexComponent description =
            Text.builder()
                    .text(reminder.getDescription())
                    .wrap(true)
                    .align(FlexAlign.CENTER)
                    .size(FlexFontSize.Md)
                    .build();

    Function<String, FlexComponent> dateTimeText = (text) -> Text.builder()
            .flex(2)
            .text(text)
            .size(FlexFontSize.LG)
            .weight(Text.TextWeight.BOLD)
            .build();

    Function<String, FlexComponent> dateTimeValue = (text) -> Text.builder()
            .flex(5)
            .text(text)
            .size(FlexFontSize.LG)
            .wrap(true)
            .build();

    FlexComponent date = dateTimeText.apply("Date :");

    FlexComponent dateValue = dateTimeValue.apply(dateString);

    FlexComponent time = dateTimeText.apply("Time :");

    FlexComponent timeValue = dateTimeValue.apply(timeString);

    FlexComponent dateBox =
            Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(date, dateValue)
                    .margin(FlexMarginSize.LG)
                    .build();

    FlexComponent timeBox =
            Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(time, timeValue)
                    .margin(FlexMarginSize.MD)
                    .build();

    return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(title, description, dateBox, timeBox)
            .build();
  }

  private Box getFooter() {
    Group group = reminder.getGroup();
    return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .content(
                    Text.builder()
                            .text(group.getGroupName())
                            .size(FlexFontSize.Md)
                            .weight(Text.TextWeight.BOLD)
                            .align(FlexAlign.END)
                            .offsetBottom(FlexOffsetSize.SM)
                            .offsetEnd(FlexOffsetSize.MD)
                            .build()
            )
            .build();
  }

  /**
   * build a flex message.
   *
   * @return Bubble message
   */
  public FlexContainer getFlexMessage() {
    return Bubble.builder()
            .header(getHeader())
            .body(getBody())
            .footer(getFooter())
            .build();
  }

}

