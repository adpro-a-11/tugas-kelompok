package com.remi.remimessaging.model;

public class Announcement {

  private String announcementId; //key format = <group_id>-<title>

  private String description;

  private String title;

  private Group group;

  public Announcement() {

  }

  /**
   * Constructor for announcement.
   *
   * @param announcementId announcement id
   * @param description    announcement desc
   * @param title          announcement title
   * @param group          group announcement
   */
  public Announcement(String announcementId, String description, String title, Group group) {
    this.announcementId = announcementId;
    this.description = description;
    this.title = title;
    this.group = group;
  }

  public String getAnnouncementId() {
    return announcementId;
  }

  public void setAnnouncementId(String announcementId) {
    this.announcementId = announcementId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Group getGroup() {
    return group;
  }

  public void setGroup(Group group) {
    this.group = group;
  }
}
