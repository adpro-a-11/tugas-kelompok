package com.remi.remimessaging.model;

import java.util.Set;

public class Group {

  private int groupId;

  private String groupName;

  private User user;

  private String description;

  private Set<User> groupMember;

  public Group() {

  }

  /**
   * Constructor for creating group.
   * @param groupId group id
   * @param groupName group name
   * @param user user who owned group
   * @param description group desc
   */
  public Group(int groupId, String groupName, User user, String description) {
    this.groupId = groupId;
    this.groupName = groupName;
    this.user = user;
    this.description = description;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Set<User> getGroupMember() {
    return groupMember;
  }

  public void setGroupMember(Set<User> groupMember) {
    this.groupMember = groupMember;
  }

  public int getGroupMemberSize() {
    return groupMember.size();
  }

  /**
   * Add user to group.
   * @param user user that want to be added
   */
  public void addGroupMember(User user) {
    if (!groupMember.contains(user)) {
      groupMember.add(user);
    }
  }

  public void removeGroupMember(User user) {
    groupMember.remove(user);
  }
}
