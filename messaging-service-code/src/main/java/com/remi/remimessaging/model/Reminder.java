package com.remi.remimessaging.model;

import java.util.Date;
import java.util.List;

public class Reminder {

  private String reminderId; //key format = <group_id>-<title>

  private String title;

  private Group group;

  private Date dateTime;

  private String description;

  private List<String> activationSetting;

  /**
   * Constructor for creating reminder.
   * @param reminderId reminder id
   * @param title reminder title
   * @param group reminder group owner
   * @param dateTime reminder target date
   * @param description reminder desc
   * @param activationSetting list of activation settings
   */
  public Reminder(
          String reminderId,
          String title,
          Group group, Date dateTime,
          String description,
          List<String> activationSetting) {
    this.reminderId = reminderId;
    this.title = title;
    this.group = group;
    this.dateTime = dateTime;
    this.description = description;
    this.activationSetting = activationSetting;
  }

  /**
   * Empty constructor.
   */
  public Reminder() {

  }

  public boolean isDone() {
    return activationSetting.isEmpty();
  }

  /**
   * Add activation settings.
   * @param activation string activation
   */
  public void addActivation(String activation) {
    if (!activationSetting.contains(activation)) {
      activationSetting.add(activation);
    }
  }

  public void removeActivation(String activation) {
    activationSetting.remove(activation);
  }

  public String getReminderId() {
    return reminderId;
  }

  public void setReminderId(String reminderId) {
    this.reminderId = reminderId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getDateTime() {
    return dateTime;
  }

  public void setDateTime(Date dateTime) {
    this.dateTime = dateTime;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Group getGroup() {
    return group;
  }

  public void setGroup(Group group) {
    this.group = group;
  }

  public List<String> getActivationSetting() {
    return activationSetting;
  }

  public void setActivationSetting(List<String> activationSetting) {
    this.activationSetting = activationSetting;
  }
}
