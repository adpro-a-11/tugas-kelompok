package com.remi.remimessaging.model;

import java.util.Set;


public class User {

  private String userId;

  private String name;

  private Set<Group> listGroup;

  public User() {

  }

  /**
   * Constructor for creating user.
   * @param userId line user Id
   * @param name user name
   * @param listGroup list group of user
   */
  public User(String userId, String name, Set<Group> listGroup) {
    this.userId = userId;
    this.name = name;
    this.listGroup = listGroup;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setListGroup(Set<Group> listGroup) {
    this.listGroup = listGroup;
  }

  public int getListGroupSize() {
    return listGroup.size();
  }

  /**
   * add group to user.
   * @param group group that want to be added
   */
  public void addGroup(Group group) {
    if (!listGroup.contains(group)) {
      listGroup.add(group);
      group.addGroupMember(this);
    }
  }

  /**
   * remove group from user.
   * @param group object group that want to be deleted
   */
  public void removeGroup(Group group) {
    if (listGroup.contains(group)) {
      listGroup.remove(group);
      group.removeGroupMember(this);
    }
  }
}
