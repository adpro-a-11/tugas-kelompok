package com.remi.remimessaging.utils;

import java.util.Map;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class PostBackMessageData extends UserData {
  private Map<String,String> postBackParams;
  private String postBackData;

  /**
   * Constructor.
   * @param userId userId
   * @param replyToken replyToken
   * @param postBackParams Params
   * @param postBackData messageData
   */
  public PostBackMessageData(
          String userId,
          String replyToken,
          Map<String, String> postBackParams,
          String postBackData) {
    super(userId, replyToken);
    this.postBackParams = postBackParams;
    this.postBackData = postBackData;
  }
}
