package com.remi.remimessaging.utils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class TextMessageData extends UserData {
  private String message;

  public TextMessageData(String userId, String replyToken, String message) {
    super(userId, replyToken);
    this.message = message;
  }
}
