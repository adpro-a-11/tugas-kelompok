package com.remi.remimessaging.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserData {
  private String userId;
  private String replyToken;

  public UserData(String userId, String replyToken) {
    this.userId = userId;
    this.replyToken = replyToken;
  }
}
