package com.remi.remimessaging.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.remi.remimessaging.feign.CoreFeign;
import com.remi.remimessaging.model.Group;
import com.remi.remimessaging.model.Reminder;
import com.remi.remimessaging.model.User;

import java.util.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(controllers = MainController.class)
class MainControllerTest {

  @Autowired
  public MockMvc mockMvc;

  @MockBean
  private LineMessagingClient lineMessagingClient;

  @MockBean
  private CoreFeign coreFeign;

  @Test
  public void sendReplyMessage() throws Exception {
    mockMvc.perform(post("/message/reply")
    .content("{\"asdasd\":\"asdasdsadasdasd\"}")
    .contentType(MediaType.APPLICATION_JSON));
  }

  @Test
  public void sendPushMessage() throws Exception {
    mockMvc.perform(post("/message/push")
            .content("{\"asdasd\":\"asdasdsadasdasd\"}")
            .contentType(MediaType.APPLICATION_JSON));
  }

  @Test
  public void sendReminderMessage() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    Map<String, Reminder> map = new HashMap<>();
    User user = new User();
    user.setListGroup(new HashSet<>());
    user.setName("asdasdsad");
    user.setUserId("asdasdasd");
    Group group = new Group();
    group.setGroupName("asdasd");
    group.setGroupMember(new HashSet<>());
    group.setUser(user);
    group.setGroupId(1);
    group.setDescription("asdasdsa");
    Reminder reminder = new Reminder();
    reminder.setGroup(group);
    reminder.setDateTime(new Date());
    reminder.setTitle("sdgsdgsdg");
    reminder.setActivationSetting(new ArrayList<>());

    map.put("asdasdsad",reminder);

    String data = mapper.writeValueAsString(map);
    System.out.println(data);

    mockMvc.perform(post("/message/reminder/D")
    .contentType(MediaType.APPLICATION_JSON)
    .content(data));
  }



}