package com.remi.remimessaging.controller;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;

import java.text.ParseException;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
class MessagingControllerTest {

  @MockBean
  private MessagingController messageController;

  @Test
  void handleTextEvent() {
    MessageEvent<TextMessageContent> messageEvent = spy(new MessageEvent<TextMessageContent>(
            "123",
            new UserSource("asdasd"),
            new TextMessageContent("123", "asdasd"),
            new Date().toInstant()
    ));
    messageController.handleTextEvent(messageEvent);
  }

  @Test
  void handlePostBackEvent() {
    PostbackEvent postbackEvent = spy(
            new PostbackEvent(
                    "asd",
                    new UserSource("asdas"),
                    new PostbackContent("asd", null),
                    new Date().toInstant()
            )
    );

    try {
      messageController.handlePostBackEvent(postbackEvent);
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }
}