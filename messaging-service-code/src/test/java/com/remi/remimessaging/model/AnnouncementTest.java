package com.remi.remimessaging.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnnouncementTest {

  public Announcement announcement;

  @BeforeEach
  public void setUp(){
    announcement = new Announcement();
  }

  @Test
  void getAnnouncementId() {
    announcement.setAnnouncementId("asd");
    assertEquals("asd",announcement.getAnnouncementId());
  }

  @Test
  void setAnnouncementId() {
    announcement.setAnnouncementId("asd");
    assertEquals("asd",announcement.getAnnouncementId());
  }

  @Test
  void getDescription() {
    announcement.setDescription("asd");
    assertEquals("asd",announcement.getDescription());
  }

  @Test
  void setDescription() {
    announcement.setDescription("asd");
    assertEquals("asd",announcement.getDescription());
  }

  @Test
  void getTitle() {
    announcement.setTitle("asd");
    assertEquals("asd",announcement.getTitle());
  }

  @Test
  void setTitle() {
    announcement.setTitle("asd");
    assertEquals("asd",announcement.getTitle());
  }

  @Test
  void getGroup() {
    Group group = new Group();
    announcement.setGroup(group);
    assertEquals(group,announcement.getGroup());
  }

  @Test
  void setGroup() {
    Group group = new Group();
    announcement.setGroup(group);
    assertEquals(group,announcement.getGroup());
  }
}