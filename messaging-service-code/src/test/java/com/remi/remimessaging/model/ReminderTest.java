package com.remi.remimessaging.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ReminderTest {

  private Reminder reminder;

  @BeforeEach
  private void setUp() {
    reminder = new Reminder();
  }

  @Test
  void addActivationIfActivationNotExists() {
    Reminder reminder = new Reminder();
    reminder.setActivationSetting(new ArrayList<>());
    reminder.addActivation("test");

    assertEquals(1, reminder.getActivationSetting().size());
  }

  @Test
  void removeActiavtion() {
    Reminder reminder = new Reminder();
    reminder.setActivationSetting(new ArrayList<>());
    reminder.addActivation("test");

    reminder.removeActivation("test");
    assertEquals(0, reminder.getActivationSetting().size());
  }

  @Test
  void isDone() {
    Reminder reminder = new Reminder();
    reminder.setActivationSetting(new ArrayList<>());
    assertTrue(reminder.isDone());
  }

  @Test
  void getReminderId() {
    reminder.setReminderId("asd");
    assertEquals("asd", reminder.getReminderId());
  }

  @Test
  void setReminderId() {
    reminder.setReminderId("asd");
    assertEquals("asd", reminder.getReminderId());
  }

  @Test
  void getTitle() {
    reminder.setTitle("asd");
    assertEquals("asd", reminder.getTitle());
  }

  @Test
  void setTitle() {
    reminder.setTitle("asd");
    assertEquals("asd", reminder.getTitle());
  }

  @Test
  void getDateTime() {
    Date date = new Date();
    reminder.setDateTime(date);
    assertEquals(date, reminder.getDateTime());
  }

  @Test
  void setDateTime() {
    Date date = new Date();
    reminder.setDateTime(date);
    assertEquals(date, reminder.getDateTime());
  }

  @Test
  void getDescription() {
    reminder.setDescription("asd");
    assertEquals("asd", reminder.getDescription());
  }

  @Test
  void setDescription() {
    reminder.setDescription("asd");
    assertEquals("asd", reminder.getDescription());
  }

  @Test
  void getGroup() {
    Group group = new Group();
    reminder.setGroup(group);
    assertEquals(group, reminder.getGroup());
  }

  @Test
  void setGroup() {
    Group group = new Group();
    reminder.setGroup(group);
    assertEquals(group, reminder.getGroup());
  }

  @Test
  void getActivationSetting() {
    List<String> list = new ArrayList<>();
    reminder.setActivationSetting(new ArrayList<>());
    assertEquals(list, reminder.getActivationSetting());
  }

  @Test
  void setActivationSetting() {
    List<String> list = new ArrayList<>();
    reminder.setActivationSetting(new ArrayList<>());
    assertEquals(list, reminder.getActivationSetting());
  }
}