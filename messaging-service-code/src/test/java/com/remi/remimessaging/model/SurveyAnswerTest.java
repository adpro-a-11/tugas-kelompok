package com.remi.remimessaging.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SurveyAnswerTest {

  private SurveyAnswer surveyAnswer;

  @BeforeEach
  void setUp() {
    surveyAnswer = new SurveyAnswer();
  }

  @Test
  void getSurveyId() {
    surveyAnswer.setSurveyId("asdasd");
    assertEquals("asdasd", surveyAnswer.getSurveyId());
  }

  @Test
  void setSurveyId() {
    surveyAnswer.setSurveyId("asdasd");
    assertEquals("asdasd", surveyAnswer.getSurveyId());
  }

  @Test
  void getAnswer() {
    surveyAnswer.setAnswer("asdasd");
    assertEquals("asdasd", surveyAnswer.getAnswer());
  }

  @Test
  void setAnswer() {
    surveyAnswer.setAnswer("asdasd");
    assertEquals("asdasd", surveyAnswer.getAnswer());
  }

  @Test
  void getSurvey() {
    Survey survey = new Survey();
    surveyAnswer.setSurvey(survey);
    assertEquals(survey, surveyAnswer.getSurvey());
  }

  @Test
  void setSurvey() {
    Survey survey = new Survey();
    surveyAnswer.setSurvey(survey);
    assertEquals(survey, surveyAnswer.getSurvey());
  }

  @Test
  void getUser() {
    User user = new User();
    surveyAnswer.setUser(user);
    assertEquals(user, surveyAnswer.getUser());
  }

  @Test
  void setUser() {
    User user = new User();
    surveyAnswer.setUser(user);
    assertEquals(user, surveyAnswer.getUser());
  }
}