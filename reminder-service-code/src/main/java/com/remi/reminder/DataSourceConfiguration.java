package com.remi.reminder;

import javax.sql.DataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration {

  /**
   * database config.
   * @return datasource
   */
  @Bean
  public DataSource getDataSource() {
    DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
    if (System.getenv("PRODUCTION") != null) {
      dataSourceBuilder.url(System.getenv("JDBC_DATABASE_URL"));
      dataSourceBuilder.username(System.getenv("JDBC_DATABASE_USERNAME"));
      dataSourceBuilder.password(System.getenv("JDBC_DATABASE_PASSWORD"));
    } else if (System.getenv("CI") != null) {
      dataSourceBuilder.url("jdbc:h2:mem:testdb");
      dataSourceBuilder.driverClassName("org.h2.Driver");
    } else {
      dataSourceBuilder.url("jdbc:postgresql://localhost:5432/remi_adprog"); //Isi db url disini
      dataSourceBuilder.username("test_user"); //Isi username db
      dataSourceBuilder.password("test123"); //Isi password db
    }
    return dataSourceBuilder.build();
  }
}
