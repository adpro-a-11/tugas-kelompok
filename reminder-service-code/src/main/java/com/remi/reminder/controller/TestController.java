package com.remi.reminder.controller;

import com.remi.reminder.model.Group;
import com.remi.reminder.model.Reminder;
import com.remi.reminder.model.User;
import com.remi.reminder.model.repository.GroupRepository;
import com.remi.reminder.model.repository.ReminderRepository;
import com.remi.reminder.model.repository.UserRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

  @Autowired
  private GroupRepository groupRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ReminderRepository reminderRepository;

  /**
   * addReminder controller.
   *
   * @param minute theminute
   * @param data   data
   * @return String
   */
  @PostMapping("/add-reminder/{minutes}/")
  public String addReminder(
          @PathVariable("minutes") int minute,
          @RequestBody Map<String, Object> data) {
    Group group;
    if (groupRepository.findAll().isEmpty()) {
      User user;
      if (userRepository.findAll().isEmpty()) {
        user = new User();
        user.setName("Dipta");
        user.setUserId("asdasdasdsad");
        userRepository.save(user);
      } else {
        user = userRepository.getOne("asdasdasdsad");
      }

      group = new Group();
      group.setGroupName("Group 1");
      group.setGroupMember(new HashSet<>());
      group.setUser(user);

      groupRepository.save(group);

      group.addGroupMember(user);

      groupRepository.save(group);
    } else {
      group = groupRepository.findAll().get(0);
    }

    ArrayList<String> listActivation = (ArrayList<String>) data.get("activation_list");
    Date date = new Date();
    date.setMinutes(date.getMinutes() + minute);


    Reminder reminder = new Reminder();
    reminder.setGroup(group);
    reminder.setActivationSetting(listActivation);
    reminder.setDateTime(date);

    String title = data.get("title").toString();
    String desc = data.get("description").toString();
    reminder.setTitle(title);
    reminder.setReminderId(
            String.format("%d-%s",
                    group.getGroupId(), title.toLowerCase()));
    reminder.setDescription(desc);

    reminderRepository.save(reminder);

    return "Success";
  }

  //  @GetMapping("/test-messaging-controller")
  //  public String testMessagingController(){
  //    Group group = new Group();
  //    group.setGroupName("Test Group");
  //    User user = new User();
  //    user.setName("asdasd");
  //    user.setListGroup(new HashSet<>());
  //
  //    Set<User> users = new HashSet<>(Arrays.asList(user));
  //
  //    group.setGroupMember(users);
  //    Reminder reminder = new Reminder();
  //    reminder.setTitle("Test Reminder");
  //    reminder.setDescription("asdasdasdasdsad");
  //    reminder.setActivationSetting(new ArrayList<>());
  //    reminder.setDateTime(new Date());
  //    reminder.setGroup(group);
  //
  //    HashMap<String,Reminder> reminderHashMap = new HashMap<>();
  //
  //    return messagingFeign.sendReminderMessage(reminderHashMap,"D");
  //  }
}
