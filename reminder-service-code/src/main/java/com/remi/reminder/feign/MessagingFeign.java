package com.remi.reminder.feign;

import com.remi.reminder.model.Reminder;
import java.util.Map;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("remi-messaging")
public interface MessagingFeign {
  @PostMapping("/message/reminder/{activation}")
  public String sendReminderMessage(
          @RequestBody Map<String, Reminder> reminderData,
          @PathVariable("activation") String activationSetting);

  @PostMapping("/message/push")
  public String sendPushMessage(@RequestBody Map<String, String> data);
}
