package com.remi.reminder.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "group_reminder")
public class Group {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private int groupId;

  @Column(name = "name")
  private String groupName;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "admin_id", referencedColumnName = "id")
  private User user;

  @Column(name = "description")
  private String description;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
          name = "group_member",
          joinColumns = @JoinColumn(name = "group_id"),
          inverseJoinColumns = @JoinColumn(name = "user_id")
  )
  private Set<User> groupMember;

  public Group() {

  }

  /**
   * Constructor for creating group.
   * @param groupId group id
   * @param groupName group name
   * @param user user who owned group
   * @param description group desc
   */
  public Group(int groupId, String groupName, User user, String description) {
    this.groupId = groupId;
    this.groupName = groupName;
    this.user = user;
    this.description = description;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Set<User> getGroupMember() {
    return groupMember;
  }

  public void setGroupMember(Set<User> groupMember) {
    this.groupMember = groupMember;
  }

  public int getGroupMemberSize() {
    return groupMember.size();
  }

  /**
   * Add user to group.
   * @param user user that want to be added
   */
  public void addGroupMember(User user) {
    if (!groupMember.contains(user)) {
      groupMember.add(user);
    }
  }

  public void removeGroupMember(User user) {
    groupMember.remove(user);
  }
}
