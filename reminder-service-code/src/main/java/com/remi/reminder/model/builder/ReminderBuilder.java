package com.remi.reminder.model.builder;

import com.remi.reminder.model.Group;
import com.remi.reminder.model.Reminder;
import java.util.Date;
import java.util.List;

public class ReminderBuilder {
  private String reminderId;
  private String title;
  private Group group;
  private Date dateTime;
  private String description;
  private List<String> activationSetting;

  public ReminderBuilder setReminderId(String reminderId) {
    this.reminderId = reminderId;
    return this;
  }

  public ReminderBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  public ReminderBuilder setGroup(Group group) {
    this.group = group;
    return this;
  }

  public ReminderBuilder setDateTime(Date dateTime) {
    this.dateTime = dateTime;
    return this;
  }

  public ReminderBuilder setDescription(String description) {
    this.description = description;
    return this;
  }

  public ReminderBuilder setActivationSetting(List<String> activationSetting) {
    this.activationSetting = activationSetting;
    return this;
  }

  public Reminder createReminder() {
    return new Reminder(reminderId, title, group, dateTime, description, activationSetting);
  }
}