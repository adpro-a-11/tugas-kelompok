package com.remi.reminder.service;

import com.remi.reminder.feign.MessagingFeign;
import com.remi.reminder.model.Reminder;
import com.remi.reminder.model.repository.ReminderRepository;
import com.remi.reminder.utils.DateUtils;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ReminderTimeWatcher extends TimeWatcher {

  private final ReminderRepository reminderRepository;
  private final MessagingFeign messagingFeign;
  private final Timer timer;
  private final DateUtils dateUtils = DateUtils.getInstance();
  private final Logger logger = LoggerFactory.getLogger(ReminderTimeWatcher.class);

  @Override
  public void run() {
    notifyWatcher(new Date());
  }

  private ReminderTimeWatcher(
          ReminderRepository reminderRepository,
          MessagingFeign messagingFeign) {
    timer = new Timer();
    this.reminderRepository = reminderRepository;
    this.messagingFeign = messagingFeign;
    startTimer();
  }

  public void stopTimer() {
    timer.cancel();
  }

  public void startTimer() {
    timer.schedule(this, new Date(), 60 * 1000);
  } //Notify every 5 minutes

  public long countActiveReminder() {
    return reminderRepository.count();
  }

  @Override
  public void notifyWatcher(Date date) {
    Date normalizeDate = dateUtils.normalizeDate(date);
    logger.info(String.format("Notify date %s", normalizeDate.toString()));
    List<Reminder> reminderList = reminderRepository
            .findRemindersByDateTimeGreaterThanEqual(normalizeDate);
    for (Reminder obj : reminderList) {
      ReminderWatcher reminderWatcher = new ReminderWatcher(
              obj,
              reminderRepository,
              messagingFeign
      );
      reminderWatcher.checkTime(normalizeDate);
    }
    System.gc();
  }
}
