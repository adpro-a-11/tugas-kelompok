package com.remi.reminder.service;

import com.remi.reminder.feign.MessagingFeign;
import com.remi.reminder.model.Group;
import com.remi.reminder.model.Reminder;
import com.remi.reminder.model.User;
import com.remi.reminder.model.repository.ReminderRepository;
import com.remi.reminder.utils.DateUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReminderWatcher implements Watcher {
  private final Reminder reminder;
  private final DateUtils dateUtils = DateUtils.getInstance();
  private final ReminderRepository reminderRepository;
  private final MessagingFeign messagingFeign;
  private final Logger logger = LoggerFactory.getLogger(ReminderWatcher.class);

  /**
   * Constructor.
   *
   * @param reminder           the reminder
   * @param reminderRepository reminder repository
   */
  public ReminderWatcher(
          Reminder reminder,
          ReminderRepository reminderRepository,
          MessagingFeign messagingFeign) {
    this.reminder = reminder;
    this.reminderRepository = reminderRepository;
    this.messagingFeign = messagingFeign;
  }

  /**
   * For sending message.
   *
   * @param activationSetting the activationSetting
   */
  public void sendMessage(String activationSetting) {
    Group reminderGroup = reminder.getGroup();
    Set<User> groupMember = reminderGroup.getGroupMember();
    Map<String,Reminder> map = new HashMap<>();

    for (User usr : groupMember) {
      String userId = usr.getUserId();
      map.put(userId, reminder);
    }

    CompletableFuture.runAsync(() -> {
      messagingFeign.sendReminderMessage(
          map,activationSetting
      );
      logger.info(
              String.format("Done send reminder %s in group %s at %s",
                      reminder.getTitle(),reminderGroup.getGroupName(),new Date()));
    });
    System.gc();
  }

  @Override
  public void checkTime(Date dateNow) {
    List<String> activationList = reminder.getActivationSetting();
    Date reminderDate = dateUtils.normalizeDate(reminder.getDateTime());

    if (dateNow.equals(reminderDate)) {
      sendMessage("D");
    } else {
      Optional<String> activeSettings = activationList
              .stream()
              .filter(obj -> {
                Date subtractedDate =
                        subtractDateWithActivationSettings(reminderDate, obj);
                return subtractedDate.equals(dateNow);
              }).findAny(); 

      if (activeSettings.isPresent()) {
        String activationSetting = activeSettings.get().toUpperCase();
        sendMessage(activationSetting);
        reminder.removeActivation(activationSetting);
        reminderRepository.save(reminder);
      }
    }
  }

  /**
   * subtract activation settings with reminder date.
   *
   * @param date       reminder date
   * @param activation activation settings
   * @return subtracted date
   */
  public Date subtractDateWithActivationSettings(Date date, String activation) {
    String newActivation = activation.toUpperCase();
    String[] activationData = newActivation.split("-");
    int time = Integer.parseInt(activationData[1]);

    switch (activationData[0]) {
      case "H":
        return dateUtils.subtractDateByDay(date, time);
      case "T":
        return dateUtils.subtractDateByMinute(date, time);
      default:
        return date;
    }
  }
}
