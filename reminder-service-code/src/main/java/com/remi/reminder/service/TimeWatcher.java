package com.remi.reminder.service;

import java.util.Date;
import java.util.TimerTask;

public abstract class TimeWatcher extends TimerTask {
  public abstract void notifyWatcher(Date date);

}
