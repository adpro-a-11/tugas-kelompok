package com.remi.reminder.service;

import java.util.Date;

public interface Watcher {
  public void checkTime(Date date);
}
