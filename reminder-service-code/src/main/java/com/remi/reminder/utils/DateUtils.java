package com.remi.reminder.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
  private static final DateUtils singleton = new DateUtils();
  private final Calendar calendar = Calendar.getInstance();

  /**
   * normalize milisecond and second of date.
   * @param date input date
   * @return normalize date
   */
  public Date normalizeDate(Date date) {
    calendar.setTime(date);

    //Normalize date
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  /**
   * subtract date with some mamount of hours.
   * @param date input date
   * @param hours ammount of subtracted hours
   * @return subtracted date
   */
  public Date subtractDateByHours(Date date, int hours) {
    calendar.setTime(date);

    //Subtract Hour
    calendar.add(Calendar.HOUR, -hours);

    return calendar.getTime();
  }

  /**
   * subtract input date with some ammount of minute.
   * @param date input date
   * @param minute ammount of minute
   * @return subtracted date
   */
  public Date subtractDateByMinute(Date date, int minute) {
    calendar.setTime(date);

    //Subtract Minute
    calendar.add(Calendar.MINUTE, -minute);

    return calendar.getTime();
  }

  /**
   * substract input date with some ammount of day.
   * @param date Input Date
   * @param day Ammount of subtract day
   * @return subtracted date
   */
  public Date subtractDateByDay(Date date, int day) {
    calendar.setTime(date);

    //Subtract Date
    calendar.add(Calendar.DATE, -day);

    return calendar.getTime();
  }

  /**
   * get Singleton Object.
   * @return DateUtils object
   */
  public static DateUtils getInstance() {
    return singleton;
  }
}
