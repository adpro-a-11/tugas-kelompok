package com.remi.reminder.controller;

import com.remi.reminder.model.repository.GroupRepository;
import com.remi.reminder.model.repository.ReminderRepository;
import com.remi.reminder.model.repository.UserRepository;

import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(controllers = TestController.class)
class TestControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private GroupRepository groupRepository;

  @MockBean
  private UserRepository userRepository;

  @MockBean
  private ReminderRepository reminderRepository;

  @Test
  public void testAddReminder() throws Exception {
    mockMvc.perform(post("/add-reminder/6/")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n" +
                    "\t\"title\":\"Test2\",\n" +
                    "\t\"description\":\"Some reminder 2\",\n" +
                    "\t\"activation_list\":[\n" +
                    "\t\t\"T-2\",\"T-1\"]\n" +
                    "}"));
  }


}