package com.remi.reminder.message;

import com.remi.reminder.model.Group;
import com.remi.reminder.model.Reminder;
import com.remi.reminder.model.builder.ReminderBuilder;

import java.util.Date;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.spy;

class ReminderMessageTest {

  @Test
  void getFlexMessage() {
    Group group = spy(new Group());
    group.setGroupName("asd");
    Reminder reminder = spy(new ReminderBuilder().createReminder());
    reminder.setGroup(group);
    reminder.setDateTime(new Date());
    reminder.setDescription("asd");
    reminder.setTitle("asd");
    ReminderMessage reminderMessage = new ReminderMessage("H-7", reminder);

    reminderMessage.getFlexMessage();
    Mockito.verify(reminder, Mockito.atLeastOnce()).getDateTime();
    Mockito.verify(reminder, Mockito.atLeastOnce()).getGroup();
    Mockito.verify(reminder, Mockito.atLeastOnce()).getDescription();
    Mockito.verify(reminder, Mockito.atLeastOnce()).getTitle();
    Mockito.verify(group, atLeastOnce()).getGroupName();
  }
}