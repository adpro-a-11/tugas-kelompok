package com.remi.reminder.model;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GroupTest {

  private Group group;

  @BeforeEach
  private void setUp() {
    group = new Group();
  }

  @Test
  void addGroupMemberTest() {
    User user = new User();
    Group group = new Group();
    group.setGroupMember(new HashSet<>());
    group.addGroupMember(user);

    assertEquals(1, group.getGroupMemberSize());
  }

  @Test
  void removeGroupMemberTest() {
    User user = new User();
    Group group = new Group();
    group.setGroupMember(new HashSet<>());
    group.addGroupMember(user);

    group.removeGroupMember(user);
    assertEquals(0, group.getGroupMemberSize());
  }

  @Test
  void getGroupId() {
    group.setGroupId(1);
    assertEquals(1, group.getGroupId());
  }

  @Test
  void setGroupId() {
    group.setGroupId(1);
    assertEquals(1, group.getGroupId());
  }

  @Test
  void getGroupName() {
    group.setGroupName("asdasd");
    assertEquals("asdasd", group.getGroupName());
  }

  @Test
  void setGroupName() {
    group.setGroupName("asdasd");
    assertEquals("asdasd", group.getGroupName());
  }

  @Test
  void getDescription() {
    group.setDescription("asdasd");
    assertEquals("asdasd", group.getDescription());
  }

  @Test
  void setDescription() {
    group.setDescription("asdasd");
    assertEquals("asdasd", group.getDescription());
  }

  @Test
  void getUser() {
    User user = new User();
    group.setUser(user);
    assertEquals(user, group.getUser());
  }

  @Test
  void setUser() {
    User user = new User();
    group.setUser(user);
    assertEquals(user, group.getUser());
  }

  @Test
  void getGroupMember() {
    Set<User> userSet = new HashSet<>();
    group.setGroupMember(userSet);
    assertEquals(userSet, group.getGroupMember());
  }

  @Test
  void setGroupMember() {
    Set<User> userSet = new HashSet<>();
    group.setGroupMember(userSet);
    assertEquals(userSet, group.getGroupMember());
  }

  @Test
  void getGroupMemberSize() {
    Set<User> userSet = new HashSet<>();
    group.setGroupMember(userSet);
    assertEquals(0, group.getGroupMemberSize());
  }
}