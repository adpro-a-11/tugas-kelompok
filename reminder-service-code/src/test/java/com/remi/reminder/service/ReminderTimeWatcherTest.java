package com.remi.reminder.service;

import com.remi.reminder.model.repository.ReminderRepository;
import com.remi.reminder.utils.DateUtils;

import java.util.Date;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ReminderTimeWatcherTest {

  @Mock
  private ReminderRepository reminderRepository;

  @InjectMocks
  private ReminderTimeWatcher reminderTimeWatcher;

  @Test
  public void countActiveReminder() {
    reminderTimeWatcher.countActiveReminder();

    Mockito.verify(reminderRepository, Mockito.times(1)).count();
  }

  @Test
  public void notifyWatcher() {
    Date date = new Date();
    reminderTimeWatcher.notifyWatcher(date);

    DateUtils dateUtils = DateUtils.getInstance();

    Mockito.verify(reminderRepository, Mockito.atLeastOnce()).findRemindersByDateTimeGreaterThanEqual(dateUtils.normalizeDate(date));
  }

}