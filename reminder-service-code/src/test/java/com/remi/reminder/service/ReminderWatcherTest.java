package com.remi.reminder.service;

import com.linecorp.bot.client.LineMessagingClient;
import com.remi.reminder.feign.MessagingFeign;
import com.remi.reminder.model.Group;
import com.remi.reminder.model.Reminder;
import com.remi.reminder.model.builder.ReminderBuilder;
import com.remi.reminder.model.repository.ReminderRepository;
import com.remi.reminder.utils.DateUtils;

import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class ReminderWatcherTest {

  private Reminder reminder;
  private ReminderWatcher reminderWatcher;
  private Date date;
  private Calendar calendar;

  @MockBean
  private MessagingFeign messagingFeign;

  @MockBean
  private ReminderRepository reminderRepository;

  @BeforeEach
  void setUp() {
    Group group = new Group();
    group.setGroupName("asd");
    group.setGroupMember(new HashSet<>());
    date = new Date();
    List<String> activationLists = new ArrayList<>();
    activationLists.add("H-1");

    reminder = Mockito.spy(new ReminderBuilder()
            .setDateTime(date)
            .setGroup(group)
            .setDescription("asdasdsad")
            .setActivationSetting(activationLists)
            .createReminder());

    reminderWatcher = Mockito.spy(new ReminderWatcher(reminder, reminderRepository,messagingFeign));
    calendar = Calendar.getInstance();
  }

  @Test
  void subtractDateWithActivationSettings() {
    Date temp = reminderWatcher.subtractDateWithActivationSettings(date, "T-5");
    calendar.setTime(date);
    calendar.add(Calendar.MINUTE, -5);
    assertEquals(calendar.getTime(), temp);

    temp = reminderWatcher.subtractDateWithActivationSettings(date, "H-1");
    calendar.setTime(date);
    calendar.add(Calendar.DATE, -1);
    assertEquals(calendar.getTime(), temp);

    temp = reminderWatcher.subtractDateWithActivationSettings(date, "J-5");
    assertEquals(date, temp);
  }

  @Test
  void checkTimeH1() {
    Date tempDat = new Date();
    tempDat.setDate(tempDat.getDate() - 1);
    Date date = DateUtils.getInstance().normalizeDate(tempDat);
    reminderWatcher.checkTime(date);

    Mockito.verify(reminder, Mockito.times(1)).getActivationSetting();
    Mockito.verify(reminderWatcher, atLeastOnce()).sendMessage("H-1");
  }

  @Test
  void checkTimeH1NoDesc() {
    Date tempDat = new Date();
    tempDat.setDate(tempDat.getDate() - 1);
    Date date = DateUtils.getInstance().normalizeDate(tempDat);
    reminderWatcher.checkTime(date);

    Mockito.verify(reminder, Mockito.times(1)).getActivationSetting();
    Mockito.verify(reminderWatcher, atLeastOnce()).sendMessage("H-1");
  }

  @Test
  void checkTimeToday() {
    Date date = DateUtils.getInstance().normalizeDate(new Date());
    reminderWatcher.checkTime(date);

    Mockito.verify(reminder, Mockito.times(1)).getActivationSetting();
    Mockito.verify(reminderWatcher, atLeastOnce()).sendMessage("D");
  }
}