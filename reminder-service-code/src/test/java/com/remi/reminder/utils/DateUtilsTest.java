package com.remi.reminder.utils;

import java.util.Calendar;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DateUtilsTest {

  private Date date;
  private Calendar calendar;
  private DateUtils dateUtils;

  @BeforeEach
  public void setUp() {
    date = new Date();
    calendar = Calendar.getInstance();
    dateUtils = DateUtils.getInstance();
  }

  @Test
  void normalizeDate() {
    calendar.setTime(date);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    assertEquals(calendar.getTime(), dateUtils.normalizeDate(date));
  }

  @Test
  void subtractDateByHours() {
    calendar.setTime(date);
    calendar.add(Calendar.HOUR, -2);
    assertEquals(calendar.getTime(), dateUtils.subtractDateByHours(date, 2));
  }

  @Test
  void subtractDateByMinute() {
    calendar.setTime(date);
    calendar.add(Calendar.MINUTE, -2);
    assertEquals(calendar.getTime(), dateUtils.subtractDateByMinute(date, 2));
  }

  @Test
  void subtractDateByDay() {
    calendar.setTime(date);
    calendar.add(Calendar.DATE, -2);
    assertEquals(calendar.getTime(), dateUtils.subtractDateByDay(date, 2));
  }

  @Test
  void getInstance() {
  }
}